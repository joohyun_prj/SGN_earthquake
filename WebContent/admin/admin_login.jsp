<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if(session.getAttribute("currentSessionUser")!=null){
		response.sendRedirect("./admin_mainpage.jsp");
	}
%>
<html>
<head>
<link rel="stylesheet" href="../static/css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="../static/css/template.css" type="text/css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="../static/js/jQuery.js"></script>
<script src="../static/js/bootstrap.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<style>
.form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SGN Earthquake Admin Login</title>
</head>
<body><br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<h1 class="text-center login-title">Login to access SGN Earthquake Admin</h1>
				<div class="account-wall">
					<img class="profile-img" src="../static/image/pie.jpg" alt="">
					<form class="form-signin" action="../Admin_Login_Servlet" method="POST">
						<input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
						<input type="password" class="form-control" name="password" placeholder="Password" required>
						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
						<a class="pull-right need-help" data-toggle="modal" data-target="#myModal">Contacts </a><span class="clearfix"></span>
					</form>
				</div>
				<br><br>
			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-lg">
			
			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title" style="text-align:center">Contact Information</h4>
			      </div>
			      <div class="modal-body">
			      	<p style="text-align:center"><img src="../static/image/log_sgn.png"></p>
			      	<p style="text-align:center; font-size: 20px">Servicio Geológico Nacional (SGN) de la República Dominicana</p>
			        <p style="text-align:center; font-size: 17px">Official Webpage : sgn.gob.do</p>
			        <p style="text-align:center; font-size: 17px">Phone Number : (809) 732-0363</p>
			        
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			
			  </div>
			</div>
				<%
				String status = request.getParameter("status")==null?"":request.getParameter("status");
				if(status.equals("loginfail")){
					/* If Username or Password is wrong, below message is shown */
					%>
						<div class="alert alert-danger" role="alert" style="text-align:center">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							&nbsp; Wrong Access Information Entered
						</div>
					<%
				}
				else if(status.equals("invalidaccess")){
					/* if someone try accessing adminpage by typing url without valid login, below message is shown */
					%>
						<div class="alert alert-danger" role="alert" style="text-align:center">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							&nbsp; Invalid Access
						</div>
					<%
				}
					%>
			</div>
		</div>
	</div>
</body>
</html>