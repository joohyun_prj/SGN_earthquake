<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.*, java.text.SimpleDateFormat" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="db_bean" scope="request" class="SGN.report_result.DB_beans" />

<%
	if(request.getParameter("tablename_filter").equals("report_all")){
 		String filepath = request.getParameter("filepath");
 		String filename = request.getParameter("filename");
 		if(db_bean.exportReportCSV(filepath, filename)){
 	 		response.sendRedirect("admin_report.jsp");
 	 	}
 	 	else{
 	 		throw new Exception("Occured error!");
 	 	}
 	}
 

 	else if(request.getParameter("tablename_filter").equals("report_filter")){
 		String filepath = request.getParameter("filepath");
 		String filename = request.getParameter("filename");
 		java.sql.Timestamp from_datetime = java.sql.Timestamp.valueOf(request.getParameter("from_datetime"));
 		java.sql.Timestamp to_datetime = java.sql.Timestamp.valueOf(request.getParameter("to_datetime"));		
		if(db_bean.exportReportCSV(filepath, filename, from_datetime, to_datetime)){
	 		response.sendRedirect("admin_report.jsp");
	 	}
	 	else{
	 		throw new Exception("Occured error!");
	 	}
	}

 	else if(request.getParameter("tablename_filter").equals("index_all")){
		String filepath = request.getParameter("filepath");
		String filename = request.getParameter("filename");
		if(db_bean.exportIndexCSV(filepath, filename)){
	 		response.sendRedirect("admin_index.jsp");
	 	}
	 	else{
	 		throw new Exception("Occured error!");
	 	}
	}

 	else if(request.getParameter("tablename_filter").equals("index_filter")){
 		String filepath = request.getParameter("filepath");
 		String filename = request.getParameter("filename");
 		java.sql.Timestamp from_datetime = java.sql.Timestamp.valueOf(request.getParameter("from_datetime"));
 		java.sql.Timestamp to_datetime = java.sql.Timestamp.valueOf(request.getParameter("to_datetime"));		
 		if(db_bean.exportIndexCSV(filepath, filename, from_datetime, to_datetime)){
	 		response.sendRedirect("admin_index.jsp");
	 	}
	 	else{
	 		throw new Exception("Occured error!");
	 	}
	}
%>
