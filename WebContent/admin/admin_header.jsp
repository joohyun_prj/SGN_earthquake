<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="SGN.report_result.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="db_bean" scope="request" class="SGN.report_result.DB_beans" />
<jsp:useBean id="reportdata" scope="request" class="SGN.report_result.ReportData" />
<jsp:useBean id="admin_userdata" scope="session" class="SGN.report_result.Admin_UserData" />	

<%
	if(session.getAttribute("currentSessionUser")==null){
		/* if someone try to access any admin page without valid login process, below code will send him back to login page */
		response.sendRedirect("/SGN_earthquake/admin/admin_login.jsp?status=invalidaccess");
	}
%>

<HTML>
<HEAD>
<link rel="stylesheet" href="../css/static/css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="../css/static/css/template.css" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="../static/js/jQuery.js"></script>
<script src="../static/js/bootstrap.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://fengyuanchen.github.io/js/common.js"></script>
<link href="../static/css/datepicker.css" rel="stylesheet">
<script src="../static/js/datepicker.js"></script>
<style>
/* below is about show more, show less toggle */
.morecontent span {
	display: none;
}

.morelink {
	display: block;
}
</style>

<script>
	
	$(document).ready(function() {
		// Configure/customize these variables to change show more, less toggle
		var showChar = 15; // How many characters are shown by default
		var ellipsestext = "...";
		var moretext = "Show more >";
		var lesstext = "Show less";
		$('.more').each(function() {
			var content = $(this).html();
			// load whole content to content variable
			if (content.length > showChar) {
			//if whole content is longer than showChar then, show only shortened content and append show more toggle
				var c = content.substr(0,showChar);
				//shortened part of whole content
				var h = content.substr(showChar,content.length - showChar);
				//rest part of whole content
				var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>'
							+ h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
							//generate shortened content and toggle button
				$(this).html(html);
				//insert shortened content to whole content's place
			}
		});
		$(".morelink").click(function() {
			if ($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
			}
			$(this).parent().prev().toggle();
			$(this).prev().toggle();
			return false;
		});
	});
	
	$(function() {
		//below code shows datepicker at the admin report table page and admin index table page 
		$('#from_date').datepicker({
			trigger : '#trigger_btn_from_date',
			startView : 1,
			format : 'yyyy-mm-dd'
		});
		$('#to_date').datepicker({
			trigger : '#trigger_btn_to_date',
			startView : 1,
			format : 'yyyy-mm-dd'
		});
	});
	
	function show_report_table(filter) {
		if (filter == 1) {
			/* if you click "apply filter" button at the report table page, below code set admin_report_filter.jsp as destination page*/
			document.admin_report_form.action='/SGN_earthquake/admin/admin_report_filter.jsp';
		}
		if (filter == 0) {
			/* if you click "show all" button at the report table page, below code set admin_report_all.jsp as destination page*/
			document.admin_report_form.action='/SGN_earthquake/admin/admin_report_all.jsp';
		}
		/* below code redirects user to destination page which has selected above code */
		document.admin_report_form.submit();
	}
	
	function show_index_table(filter) {
		if (filter == 1) {
			/* if you click "apply filter" button at the index table page, below code set admin_index_filter.jsp as destination page */
			document.admin_index_form.action='/SGN_earthquake/admin/admin_index_filter.jsp';
		}
		if (filter == 0) {
			/* if you click "show all" button at the report table page, below code set admin_index_all.jsp as destination page */
			document.admin_index_form.action='/SGN_earthquake/admin/admin_index_all.jsp';
		}
		/* below code redirects user to destination page which has selected above code */
		document.admin_report_form.submit();
	}
	
	function redirect_to_admin_report_main() {
		/* if you click "go back to admin report page" button, below code is executed */
		location.href="/SGN_earthquake/admin/admin_report.jsp";
	}
	
	function redirect_to_admin_index_main() {
		/* if you click "go back to admin index page" button, below code is executed */
		location.href="/SGN_earthquake/admin/admin_index.jsp";
	}
		
	function redirect_to_admin_main() {
		/* if you click "go back to admin mainpage" button, below code is executed */
		location.href="/SGN_earthquake/admin/admin_mainpage.jsp";
	}

	
	function redirect_update_index_table() {
		if (confirm("Index table data is syncronized with raw data table. do you want update?? ") == true){ //check
			location.href="/SGN_earthquake/admin/admin_update_index.jsp";
			/* If you access admin_update_index.jsp, index table will be synchronized with report table */
		}else{   // cancel
		    return;
		}
	}
	
	function redirect_reset_and_regenerate() {
		if (confirm("All content of index table is deleted and then regenerated.\nYou can't get former index data back.\n"+
				"Do you want reset and regenerate index table?") == true){ //check
			location.href="/SGN_earthquake/admin/admin_reset_and_regenerate.jsp";
			/* If you access admin_reset_and_regenerate.jsp, all the rows in index table will be deleted and index table will be synchronized with report table */
		}else{   // cancel
		    return;
		}
	}
	
</script>
