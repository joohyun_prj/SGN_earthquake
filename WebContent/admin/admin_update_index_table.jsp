<!-- 여기서 쿼리불러서 그룹  데이터 가져오고, 그거랑 날짜 같은 인덱스 가져오고, 만든함수로 갱신 -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="SGN.report_result.*, java.util.ArrayList"%>
<jsp:useBean id="reportdata" class="SGN.report_result.ReportData"/>
<jsp:useBean id="db_bean" scope="request" class="SGN.report_result.DB_beans"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	admin admin = new admin();
	ArrayList<GroupData> gd = new ArrayList<GroupData>();

	gd = db_bean.getgroupDB(db_bean.get_refresh_time()); // groupdata array list that after submit_test
	
	int i;
	for (i=0;i<gd.size();i++){
		System.out.println("================");
		System.out.println(db_bean.check_db(gd.get(i).getLat(),gd.get(i).getLng(),gd.get(i).getEarthquake_reported_datetime()));
		if(db_bean.check_db(gd.get(i).getLat(),gd.get(i).getLng(),gd.get(i).getEarthquake_reported_datetime())){ 
			// check whether there is some row that has same lat, lng and time or not, so if there is row, execute update query 
			IndexData update_set = new IndexData();
			IndexData former_set = new IndexData();
			
			former_set = db_bean.getindexDB(gd.get(i).getLat(),gd.get(i).getLng(),gd.get(i).getEarthquake_reported_datetime());
			// former_set is literally before update data set. so when we compute CDI, we will use former set because of efficiency.
			
			update_set = admin.Mid_compute_cdi(gd.get(i), former_set);
			// Mid_compute_cdi function compute CDI then return sesult IndexData arraylist.
			db_bean.update_index_db(gd.get(i).getLat(),gd.get(i).getLng(),gd.get(i).getEarthquake_reported_datetime(),update_set);
			// and next, we update it to database.
		}
		else{
			// check whether there is some row that has same lat, lng and reported_time or not, so if there is not row, execute insert query
		
			if(db_bean.index_insertDB(gd.get(i))){ // check whether insert is valid or not.
				System.out.println("index insert success~!");
			}
			else{
				System.out.println("index insert fail~!");
			}
		}
		
		db_bean.set_refresh_time();
	}
	
%>
</body>
</html>