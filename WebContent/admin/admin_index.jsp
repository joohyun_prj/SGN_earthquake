 <%@ include file="admin_header.jsp" %>
<TITLE>Index Table</TITLE>
</HEAD>

<BODY>
	<div style="margin-bottom:20px; margin-left:50px;">
		<H2>Index Table <small> Last Synchronize Time : <% out.print(db_bean.get_refresh_time()); %></small></H2>
		<div style="margin-top:15px;margin-bottom:15px">
		<!-- all this function in onclick attribute is declared in ' admin_header.jsp ' file. -->
			<button style="margin-left:10px;" class="btn btn-primary btn-lg" onclick='redirect_to_admin_main()'><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Go back to admin mainpage</button>
			<button class="btn btn-primary btn-lg" onclick='redirect_update_index_table()'>Actualizar la tabla de índice</button>
			<button class="btn btn-primary btn-lg" onclick='redirect_reset_and_regenerate()'>Restablecer y regenerar la tabla de índice</button>	
		</div>
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6" style="align:center">
				<form name="admin_index_form">
					<table class="table" style="margin-left: auto; margin-right: auto;">
						<tr>
							<td colspan="3" align="center"><div style="font-weight: bold;"><H4>Filter By Earthquake Time</H4></div></td>
						</tr>
						<tr>
							<td>From</td>
							<td>
								<div class="docs-datepicker">
									<div class="input-group">
										<input type="text" class="form-control docs-date" name="from_date" placeholder="Pick a date" id="from_date">
										<span class="input-group-btn">
											<button type="button" class="btn btn-default docs-datepicker-trigger" name="trigger" id="trigger_btn_from_date">
												<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
											</button>
										</span>
									</div>
								</div>
							</td>
							<td>
								<select class="form-control" style="width:225px" name="from_time" id="from_time">
									<option value="00:00:00">12:00 am</option>
									<option value="01:00:00"> 1:00 am</option>
									<option value="02:00:00"> 2:00 am</option>
									<option value="03:00:00"> 3:00 am</option>
									<option value="04:00:00"> 4:00 am</option>
									<option value="05:00:00"> 5:00 am</option>
									<option value="06:00:00"> 6:00 am</option>
									<option value="07:00:00"> 7:00 am</option>
									<option value="08:00:00"> 8:00 am</option>
									<option value="09:00:00"> 9:00 am</option>
									<option value="10:00:00">10:00 am</option>
									<option value="11:00:00">11:00 am</option>
									<option value="12:00:00">12:00 pm</option>
									<option value="13:00:00"> 1:00 pm</option>
									<option value="14:00:00"> 2:00 pm</option>
									<option value="15:00:00"> 3:00 pm</option>
									<option value="16:00:00"> 4:00 pm</option>
									<option value="17:00:00"> 5:00 pm</option>
									<option value="18:00:00"> 6:00 pm</option>
									<option value="19:00:00"> 7:00 pm</option>
									<option value="20:00:00"> 8:00 pm</option>
									<option value="21:00:00"> 9:00 pm</option>
									<option value="22:00:00"> 10:00 pm</option>
									<option value="23:00:00"> 11:00 pm</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>To</td>
							<td>
								<div class="docs-datepicker">
										<div class="input-group">
											<input type="text" class="form-control docs-date" name="to_date" placeholder="Pick a date" id="to_date">
											<span class="input-group-btn">
												<button type="button" class="btn btn-default docs-datepicker-trigger" name="trigger" id="trigger_btn_to_date">
													<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
												</button>
											</span>
										</div>
									</div>
							</td>
							<td>
								<select class="form-control" style="width:225px" name="to_time" id="to_time">
									<option value="00:00:00">12:00 am</option>
									<option value="01:00:00"> 1:00 am</option>
									<option value="02:00:00"> 2:00 am</option>
									<option value="03:00:00"> 3:00 am</option>
									<option value="04:00:00"> 4:00 am</option>
									<option value="05:00:00"> 5:00 am</option>
									<option value="06:00:00"> 6:00 am</option>
									<option value="07:00:00"> 7:00 am</option>
									<option value="08:00:00"> 8:00 am</option>
									<option value="09:00:00"> 9:00 am</option>
									<option value="10:00:00">10:00 am</option>
									<option value="11:00:00">11:00 am</option>
									<option value="12:00:00">12:00 pm</option>
									<option value="13:00:00"> 1:00 pm</option>
									<option value="14:00:00"> 2:00 pm</option>
									<option value="15:00:00"> 3:00 pm</option>
									<option value="16:00:00"> 4:00 pm</option>
									<option value="17:00:00"> 5:00 pm</option>
									<option value="18:00:00"> 6:00 pm</option>
									<option value="19:00:00"> 7:00 pm</option>
									<option value="20:00:00"> 8:00 pm</option>
									<option value="21:00:00"> 9:00 pm</option>
									<option value="22:00:00"> 10:00 pm</option>
									<option value="23:00:00"> 11:00 pm</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="3" align="center">
								<!-- show_report_table() function is declared in admin_header.jsp -->
								<button class="btn btn-primary btn-lg" onclick='show_index_table(1)'>Apply Filter</button>&nbsp;&nbsp;
								<button class="btn btn-primary btn-lg" onclick='show_index_table(0)'>Show All</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
	</div>
	<TABLE class="table" style="table-layout: fixed; word-break: break-all;">
		<THEAD>
			<TR style="font-weight: bold; background: #c9dff0;">
				<TD style="width: 100px">Row ID</TD>
				<TD style="width: 100px">Latitude</TD>
				<TD style="width: 100px">Longitude</TD>
				<TD style="width: 200px">Earthquake Time</TD>
				<TD style="width: 130px">Sum of Q1*Q4</TD>
				<TD style="width: 130px">Count of Q1*Q4</TD>
				<TD style="width: 130px">Sum of Q5</TD><!-- At the report form, each question has it’s different value, more important and serious questions have a higher value. And Each answers do too. We multiply both values to get a result. After we gather results from similar area and similar time, We sum those up. Sum of Q5 has summed answered question 5 -->
				<TD style="width: 130px">Count of Q5</TD><!-- Number of people who didn't choose "No es relevante(Not specified)" as an answer. We have to maintain this value for each questions, because we have to exclude reports who answered "Not specified" for calculating CWS and CDI. Any question that is replied "No es relevante(Not specified)" is excluded from the calculation of CWS and CDI. This answer has a 0 value and should be taken away in the denominator during calculation of CWS and CDI. -->
				<TD style="width: 130px">Sum of Q6</TD><!-- Detailed information is at https://earthquake.usgs.gov/data/dyfi/background.php -->
				<TD style="width: 130px">Count of Q6</TD>
				<TD style="width: 130px">Sum of Q8</TD>
				<TD style="width: 130px">Count of Q8</TD>
				<TD style="width: 130px">Sum of Q11</TD>
				<TD style="width: 130px">Count of Q11</TD>
				<TD style="width: 130px">Sum of Q12</TD>
				<TD style="width: 130px">Count of Q12</TD>
				<TD style="width: 130px">Sum of Q13</TD>
				<TD style="width: 130px">Count of Q13</TD>
				<TD style="width: 130px">Sum of Q16</TD>
				<TD style="width: 130px">Count of Q16</TD>
				<TD style="width: 50px">CWS</TD><!-- CWS = (sum of q1*q4)/(count of q1*q4) + (sum of q5)/(count of q5) + (sum of q6)/(count of q6) + (sum of q8)/(count of q8) + (sum of q11)/(count of q11) + (sum of q12)/(count of q12) + (sum of q13)/(count of q13) + (sum of q16)/(count of q16) -->
				<TD style="width: 50px">CDI</TD><!-- CDI = 3.40 ln(CWS) - 4.38 -->
				<!-- detailed calculation process are in the admin.java -->
			</TR>
		</THEAD>
	</TABLE>
</BODY>
</HTML>
