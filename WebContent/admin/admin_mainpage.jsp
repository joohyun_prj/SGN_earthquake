<%@ include file="admin_header.jsp"%>
<%
	admin_userdata = (Admin_UserData) (session.getAttribute("currentSessionUser"));
	String current_session_username = admin_userdata == null ? "" : admin_userdata.getUsername();
%>
<TITLE>Admin Mainpage</TITLE>
</HEAD>

<BODY>
	<div style="width:100%;margin-top:15%">
		<div style="" align="center">
			<H2>Admin Mainpage <small> SGN Earthquake</small></H2>
			<H4>Welcome <%= current_session_username %><!-- Show Current Username -->!</H4><br>
		</div>
		<div class="form-group" style="text-align:center">
			<!-- redirect_to_admin_report_main() function and redirect_to_admin_index_main() function are declared in admin_header.jsp -->
			<button class="btn btn-primary" onclick='redirect_to_admin_report_main()'><H4>Report Table</H4></button>&nbsp;&nbsp;
			<button class="btn btn-primary" onclick='redirect_to_admin_index_main()'><H4>Index Table</H4></button>&nbsp;&nbsp;
			<br><br><form action="../Admin_Logout_Servlet"><button class="btn btn-danger"><H4>Logout</H4></button></form>
			<!-- Admin_Logout_Servlet is Java Servlet, which is located at the Java Resources -> src -> SGN.report_result package -->
		</div>
	</div>
</BODY>
</HTML>