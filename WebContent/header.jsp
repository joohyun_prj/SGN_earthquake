<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<% response.setContentType("text/html; charset=UTF-8"); %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-es" lang="es-es" dir="ltr">



<!-- 
	@@@@@@@@@@@@@@@@@ Developer Contact information @@@@@@@@@@@@@@@@@@

	ljh940102@hanyang.ac.kr ### make map
	shl9417@naver.com 		### make algorithm
	claretta@hanyang.ac.kr 
	
	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 -->
 
 
	<head>
		<link rel="stylesheet" href="./static/css/basic.css" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<base href=".">
		<meta name="generator" content="Joomla! - Open Source Content Management">
		<title>Inicio - Servicio Geológico Nacional (SGN) de la República Dominicana es un</title>
		<link rel="stylesheet" href="./static/css/bootstrap.css" type="text/css">
		<link rel="stylesheet" href="./static/css/template.css" type="text/css">
		<link rel="stylesheet" href="./static/css/slide.css" type="text/css">
		<link rel="stylesheet" href="./static/css/map_list.css" type="text/css">
		<link rel="stylesheet" href="./static/css/back-top.css?ver=12" type="text/css">
		<link rel="stylesheet" href="./static/css/mobile.css" type="text/css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 		<link rel="stylesheet" href="./static/css/datepicker.css">
		<link rel="icon" type="image/png" href="./static/image/pie.jpg">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="./static/js/jQuery.js"></script>
		<script src="./static/js/bootstrap.js"></script>
		
  	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://fengyuanchen.github.io/js/common.js"></script>
		<script src="./static/js/datepicker.js"></script>
		<script src="./static/js/datepicker.es-ES.js"></script>
<!-- mobile animation -->
 <script>
$( document.body ).click(function() {
    $( "div.slideup" ).slideUp();
    document.getElementById('div1').className ='div1move';
	document.getElementById('div3').className ='div3move';
}
);
  </script>

		<script type="text/javascript" charset="UTF-8" src="./static/js/report_form.js"></script>
		<script type="text/javascript" charset="UTF-8" src="./static/js/back-top.js?ver=2"></script>
		<script type="text/javascript" charset="UTF-8" src="./static/js/child_window_location.js"></script>
		
	</head>
	<body>
		<!-- Validaciones -->
		<!-- menu movil -->
		<section class="container cabecera_movil visible-xs">
			<div class="row">
				<div class="col-xs-9">
					<div class="repDom text-left"> República Dominicana </div>
				</div>
				<div class="col-xs-3 menu_movil_boton text-right">
					<a id="boton_menu"><span class="glyphicon glyphicon-align-justify"></span></a>
				</div>
			</div>
			<div class="row menu_movil">
				<div class="dropdownmob" style="display: none;">
					<ul class="nav menu" style="margin-top: 3%">
						<li>
							<a href="mobinfo.jsp">INFORMACIONES</a>
						</li>
						<li>
							<a href="report.jsp">Reporte</a>
						</li>
						<li>	
							<a href="map.jsp">MAPA DE INTENSIDAD DE TERREMOTO</a>
						</li>
					</ul>
				</div>
			</div>
		</section>
		<!-- Cabecera -->
		<header class="container cabecera">
			<div class="row">
				<div class="col-sm-8 col-xs-12 logo">
					<a href="http://sgn.gob.do/"><img src="./static/image/log_sgn.png"
						class="img-responsive"
						alt="Servicio Geológico Nacional (SGN) de la República Dominicana es un "></a>
				</div>
				<div class="col-sm-4 hidden-xs elementos-cabecera">
					<div class="repDom"> República Dominicana </div>
					<div class="clearfix"></div>
					<div class="buscador">
						<div class="search">
							<form action="http://sgn.gob.do/index.php" method="post"
								class="form-inline">
								<button class="button" onclick="this.form.searchword.focus();">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</button>
								<input name="searchword" id="mod-search-searchword"
									maxlength="200" class="inputbox search-query" type="text"
									size="20" value="Búsqueda..."
									onblur="if (this.value==&#39;&#39;) this.value=&#39;ú...&#39;;"
									onfocus="if (this.value==&#39;ú...&#39;) this.value=&#39;&#39;;">
								<input type="hidden" name="task" value="search">
								<input type="hidden" name="option" value="com_search">
								<input type="hidden" name="Itemid" value="101">
							</form>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="menu-navegacion-facil">
						<div class="moduletable ">
							<div class="clrH3"></div>
							<div id="sUblevel1">
								<div id="sUblevel2">
									<div id="sUblevel3">
										<ul class="nav menu">
											<li><a href="http://sgn.gob.do/index.php">Inicio<span class="parentSign"></span></a></li>
											<li><a href="http://sgn.gob.do/index.php/mapa-de-sitio">Mapa desitio</a></li>
											<li><a href="http://sgn.gob.do/index.php/contacto">Contacto</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
	
					</div>
				</div>
			</div>
		</header>
		<div class="clearfix"></div>
		<section class="menu-principal hidden-xs">
			<div class="container">
				<div class="moduletable ">
					<div class="clrH3"></div>
					<div id="sUblevel1">
						<div id="sUblevel2">
							<div id="sUblevel3" align="center">
								<ul class="nav menu" style="width: 100%">
									<li style="width:200px" align="center"><a
										href="http://localhost:8080/SGN_earthquake/main.jsp">Inicio<span
											class="parentSign"></span></a></li>
									<li style="width: 200px" align="center"><a 
									href="http://localhost:8080/SGN_earthquake/report.jsp">reporte<span class="parentSign"></span></a></li>
									<li style="" align="center"><a
										href="http://localhost:8080/SGN_earthquake/map.jsp">MAPA DE INTENSIDAD DE TERREMOTO<span class="parentSign"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
<script>
    $(document).ready(function(){
       $("#boton_menu").click(function(){
    	   	var submenu = $(".dropdownmob");
    	    if(submenu.attr('style')=="display: none;"){
        	    submenu.slideDown();
    	    }
    	    else{
    	    	submenu.slideUp();
    	    }
        });
        $(".menu_movil").click(function(){
        	$(this).slideDown();
        });
    });
</script>