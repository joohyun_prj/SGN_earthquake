
<%@ include file="header.jsp" %>
<style>
input[type="radio"]{
	margin-left:25px;
}
</style>
	<section class="rastro_navegacion" style="position: absolute; z-index:2;">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ul class="breadcrumb">
						<li class="active">Est� aqu�: &nbsp;</li>
						<li>
							<a href="http://localhost:8080/SGN_earthquake/main.jsp" class="pathway">Inicio</a>
						</li>
						<li class="active">
							<span>Reporte</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<br>
<div class="report">
	<div class="container" style="width: 100%">
	<div class="report_paper">
		<form name="report_form" method="post" action="result.jsp">
			<div align="center" style="border-bottom: 1px solid grey; padding-top:2%; padding-bottom:2%;"><label><span style="color:red; font-size: 120%"><strong>Ayudanos a hacer un mapa de intensidad del movimiento dici�ndonos acerca de su percepci�n</strong></span></label></div>
	    	<div style="padding-top:2%; padding-bottom:2%;"><label><span style="color:#e36c0a; font-size: 110%"><strong>Reporte el terremoto/sismo </strong></span></label></div>
	    	<label>Se�ala tu ubicaci�n por</label><br>
	    
	    <!-- open_child_window functions are located in static/js/child_window_location.js file. -->
		
		    <div class="btn-group" role="group">
			  <button type="button" class="btn btn-default" onclick="open_child_window_location_click()">Mapa</button>
			  <button type="button" class="btn btn-default" onclick="open_child_window_location_geocoding()">Direcci�n</button>
			  <button type="button" class="btn btn-default" onclick="open_child_window_location_write()">Texto</button>
			</div>
			<input class="form-control" type="text" name="lat" id="parent_latitude" style="display :none">
		    <input class="form-control" type="text" name="lng" id="parent_longitude" style="display :none">
		    <div style="margin-top:2%;margin-bottom:2%;">
			    <label>Ubicaci�n introducida :</label><br>
		    	Latitud : <input class="form-control" type="text" name="lat" id="parent_latitude_show" readonly>
		    	Longtitud  : <input class="form-control" type="text" name="lng" id="parent_longitude_show" readonly>
		    </div>
			<div class="time">
			<div class="row">
				<div class="col-xs-6" id="mobtem">
					<div class=""><label>Fecha del terremoto</label></div>
					<div class=""><input class="form-control" type="text" id="Report_Datepicker" name="date" id="date" style="display:none;"></div><br>
					<div class="" id="datecon" style="height:30%;"></div>
				</div>
				<div class="col-xs-6">
					<div class="">
						<label>Hora del terremoto</label>
					</div>
					<div class="">
						<select class="form-control" name="time" id="time" style="">
							<option value="0:00:00">0:00 am</option>
							<option value="01:00:00"> 1:00 am</option>
							<option value="02:00:00"> 2:00 am</option>
							<option value="03:00:00"> 3:00 am</option>
							<option value="04:00:00"> 4:00 am</option>
							<option value="05:00:00"> 5:00 am</option>
							<option value="06:00:00"> 6:00 am</option>
							<option value="07:00:00"> 7:00 am</option>
							<option value="08:00:00"> 8:00 am</option>
							<option value="09:00:00"> 9:00 am</option>
							<option value="10:00:00">10:00 am</option>
							<option value="11:00:00">11:00 am</option>
							<option value="12:00:00">12:00 pm</option>
							<option value="13:00:00"> 1:00 pm</option>
							<option value="14:00:00"> 2:00 pm</option>
							<option value="15:00:00"> 3:00 pm</option>
							<option value="16:00:00"> 4:00 pm</option>
							<option value="17:00:00"> 5:00 pm</option>
							<option value="18:00:00"> 6:00 pm</option>
							<option value="19:00:00"> 7:00 pm</option>
							<option value="20:00:00"> 8:00 pm</option>
							<option value="21:00:00"> 9:00 pm</option>
							<option value="22:00:00"> 10:00 pm</option>
							<option value="23:00:00"> 11:00 pm</option>
						</select>
					</div>
				</div>
			</div>
			<br>
			</div>
			<div class="feel">
				<label> �Usted sinti� el terremoto?<!-- Did you feel it? --></label><br>
				<input type="radio" name="q1_feel" id="feel_radio1" value="5"> Si
				<input type="radio" name="q1_feel" id="feel_radio2" value="0"> No

			</div>
			<br>
			<label> El resto de este formulario es opcional. Este nos ayudara a crear un mapa de intensidades en tu localizaci�n.</label>
			<br><br>
			<div class="situation">
				<label> �D�nde estabas durante el terremoto?<!-- What was your situation during the earthquake? --></label>
				<br><input type="radio" name="q2_situation_dummy" id="situation_radio1" value="0"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q2_situation_dummy" id="situation_radio2" value="1"> Dentro de un edificio<!-- Inside a building -->
				<br><input type="radio" name="q2_situation_dummy" id="situation_radio3" value="2"> Al aire libre<!-- Outside a building  -->
				<br><input type="radio" name="q2_situation_dummy" id="situation_radio4" value="3"> Dentro de un veh�culo estacionado<!-- In a stopped vehicle -->
				<br><input type="radio" name="q2_situation_dummy" id="situation_radio5" value="4"> Dentro de un veh�culo en movimiento<!-- In a moving vehicle  -->
				<br><input type="radio" name="q2_situation_dummy" id="situation_radio6" value="5"> Otro<!-- Other  -->
				<input class="form-control" name="q2_situation_dummy_other_text" id="situation_other_text1" type="text" placeholder="por favor, especifique (menos de 30 caracteres)">
			</div>
			<br>
			<div class="asleep">
				<label> �Estabas durmiendo durante el terremoto?<!-- Were you asleep? --></label>
				<br><input type="radio" name="q3_asleep_dummy" id="asleep_radio1" value="0"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q3_asleep_dummy" id="asleep_radio2" value="1"> No
				<br><input type="radio" name="q3_asleep_dummy" id="asleep_radio3" value="2"> S�. Estaba durmiendo<!-- Slept through it -->
				<br><input type="radio" name="q3_asleep_dummy" id="asleep_radio4" value="3"> Me despert�<!-- Woke up -->
			</div>
			<br>
			<div class="others feel">
				<label> �Tambi�n lo sintieron otras personas cerca de usted?<!-- Did others nearby feel it? --></label>
				<br><input type="radio" name="q4_others" id="others_feel_radio1" value="99"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q4_others" id="others_feel_radio2" value="0"> No lo sintieron<!-- No others felt it -->
				<br><input type="radio" name="q4_others" id="others_feel_radio3" value="0.33"> Algunos lo sintieron <!-- Some felt it, most did not -->
				<br><input type="radio" name="q4_others" id="others_feel_radio4" value="0.66"> La mayor�a lo sinti�<!-- Most felt it -->
				<br><input type="radio" name="q4_others" id="others_feel_radio5" value="1"> Todos lo sintieron <!-- Everyone/almost everyone felt it -->
			</div>
			<br>
			<div class="describe">
				<label> �C�mo describes el movimiento?<!-- How would you describe the shaking? --></label>
				<br><input type="radio" name="q5_describe" id="describe_radio1" value="99"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q5_describe" id="describe_radio2" value="0"> No lo sent�<!-- Not felt -->
				<br><input type="radio" name="q5_describe" id="describe_radio3" value="1"> D�bil<!-- Weak -->
				<br><input type="radio" name="q5_describe" id="describe_radio4" value="2"> Suave<!-- Mild -->
				<br><input type="radio" name="q5_describe" id="describe_radio5" value="3"> Moderado<!-- Moderate -->
				<br><input type="radio" name="q5_describe" id="describe_radio6" value="4"> Fuerte<!-- Strong -->
				<br><input type="radio" name="q5_describe" id="describe_radio7" value="5"> Brusco<!-- Violent -->
			</div>
			<br>
			<div class="react">
				<label> �C�mo reaccionaste durante el terremoto?<!-- How did you react? --></label>
				<br><input type="radio" name="q6_react" id="react_radio1" value="99"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q6_react" id="react_radio2" value="0"> Ninguna reacci�n<!-- No reaction/not felt -->
				<br><input type="radio" name="q6_react" id="react_radio3" value="1"> Tranquilo<!-- Very little reaction -->
				<br><input type="radio" name="q6_react" id="react_radio4" value="2"> Alterado<!-- Excitement -->
				<br><input type="radio" name="q6_react" id="react_radio5" value="3"> Algo asustado<!-- Somewhat frightened -->
				<br><input type="radio" name="q6_react" id="react_radio6" value="4"> Muy asustado<!-- Very frightened -->
				<br><input type="radio" name="q6_react" id="react_radio7" value="5"> Extremadamente asustado<!-- Extremely frightened -->
			</div>
			<br>
			<div class="respond">
				<label> �Cu�l fue tu primera reacci�n?<!-- How did you respond? --></label>
				<br><input type="radio" name="q7_respond_dummy" id="respond_radio1" value="0"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q7_respond_dummy" id="respond_radio2" value="1"> No hice nada<!-- Took no action -->
				<br><input type="radio" name="q7_respond_dummy" id="respond_radio3" value="2"> Me mov� hacia la puerta/columna<!-- Moved to doorway -->
				<br><input type="radio" name="q7_respond_dummy" id="respond_radio4" value="3"> Dej� todo y me proteg�<!-- Dropped and covered -->
				<br><input type="radio" name="q7_respond_dummy" id="respond_radio5" value="4"> Corr� hacia afuera<!-- Ran outside -->
				<br><input type="radio" name="q7_respond_dummy" id="respond_radio6" value="5"> Otro<!-- Other  -->
				<input class="form-control" name="q7_respond_dummy_other_text" id="respond_other_text1" type="text" placeholder="por favor, especifique (menos de 30 caracteres)">
			</div>
			<br>
			<div class="difficult">
				<label> �Fue dif�cil estar de pie y / o caminar?<!-- Was it difficult to stand and/or walk? --></label>
				<br><input type="radio" name="q8_difficult" id="difficult_radio1" value="99"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q8_difficult" id="difficult_radio2" value="0"> No
				<br><input type="radio" name="q8_difficult" id="difficult_radio3" value="2"> Si<!-- Yes -->
			</div>
			<br>
			<div class="swing">
				<label> �Not� alg�n movimiento de las puertas u otros objetos colgantes?<!-- Did you notice any swinging of doors or other free-hanging objects? --></label>
				<br><input type="radio" name="q9_swing_dummy" id="swing_radio1" value="0"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q9_swing_dummy" id="swing_radio2" value="1"> No
				<br><input type="radio" name="q9_swing_dummy" id="swing_radio3" value="2"> S�, movimiento ligero<!-- Yes, slight swinging -->
				<br><input type="radio" name="q9_swing_dummy" id="swing_radio4" value="3"> S�, movimiento violento<!-- Yes, violent swinging -->
			</div>
			<br>
			<div class="hear">
				<label> �Escuchaste sonidos de las paredes u otros ruidos?<!-- Did you hear creaking or other noises? --></label>
				<br><input type="radio" name="q10_hear_dummy" id="hear_radio1" value="0"> No
				<br><input type="radio" name="q10_hear_dummy" id="hear_radio2" value="1"> Si, sonidos ligeros<!-- Yes, slight noise -->
				<br><input type="radio" name="q10_hear_dummy" id="hear_radio3" value="2"> Si, sonidos fuertes<!-- Yes, loud noise -->
			</div>
			<br>
			<div class="objects">
				<label> �Los objetos en los estantes, se sacudieron, tumbaron o cayeron al piso?<!-- Did objects rattle, topple over, or fall off shelves? --></label>
				<br><input type="radio" name="q11_objects" id="objects_radio2" value="99"> No es relevante <!-- Not specified -->
				<br><input type="radio" name="q11_objects" id="objects_radio3" value="1"> Se sacudieron ligeramente<!-- Rattled slightly -->
				<br><input type="radio" name="q11_objects" id="objects_radio4" value="2"> Se sacudieron bruscamente<!-- Rattled loudly -->
				<br><input type="radio" name="q11_objects" id="objects_radio5" value="3"> Algunos se tumbaron o cayeron <!-- A few toppled or fell off -->
				<br><input type="radio" name="q11_objects" id="objects_radio6" value="4"> Muchos se cayeron <!-- Many fell off -->
				<br><input type="radio" name="q11_objects" id="objects_radio7" value="5"> Todos se cayeron<!-- Nearly everything fell off -->
			</div>
			<br>
			<div class="picture">
				<label> �Los cuadros en las paredes se movieron o cayeron al piso?<!-- Did pictures on walls move or get knocked askew? --></label>
				<br><input type="radio" name="q12_picture" id="picture_radio1" value="99"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q12_picture" id="picture_radio2" value="0"> No
				<br><input type="radio" name="q12_picture" id="picture_radio3" value="1"> Se movieron, pero no cayeron <!-- Yes, but did not fall -->
				<br><input type="radio" name="q12_picture" id="picture_radio4" value="2"> Se movieron y algunos se cayeron<!-- Yes, and some fell -->
			</div>
			<br>
			<div class="furniture">
				<label> �Alg�n mueble o electrodom�stico se derrumb� o se desplaz�?<!-- Did any furniture or appliances slide, topple over, or become displaced? --></label>
				<br><input type="radio" name="q13_furniture" id="furniture_radio1" value="99"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q13_furniture" id="furniture_radio2" value="0"> No
				<br><input type="radio" name="q13_furniture" id="furniture_radio3" value="3"> S�<!-- Yes -->
			</div>
			<br>
			<div class="heavy_appliance">
				<label> �Se ha afectado alg�n aparato pesado (nevera o estufa)?<!-- Was a heavy appliance (refrigerator or range) affected? --></label>
				<br><input type="radio" name="q14_heavy_appliance_dummy" id="heavy_appliance_radio1" value="0"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q14_heavy_appliance_dummy" id="heavy_appliance_radio2" value="1"> No
				<br><input type="radio" name="q14_heavy_appliance_dummy" id="heavy_appliance_radio3" value="2"> S�, algunos de sus contenidos se cayeron<!-- Yes, some contents fell out -->
				<br><input type="radio" name="q14_heavy_appliance_dummy" id="heavy_appliance_radio4" value="3"> S�, se desplazaron un poco (algunas pulgadas)<!-- Yes, shifted by inches -->
				<br><input type="radio" name="q14_heavy_appliance_dummy" id="heavy_appliance_radio5" value="4"> S�, se desplazaron considerablemente (un pie o m�s)<!-- Yes, shifted by a foot or more -->
				<br><input type="radio" name="q14_heavy_appliance_dummy" id="heavy_appliance_radio6" value="5"> S�, se voltearon<!-- Yes, overturned -->
			</div>
			<br>
			<div class="walls_fences">
				<label> �Se da�aron paredes exteriores, cercas o mallasj?<!-- Were free-standing walls or fences damaged? --> </label>
				<br><input type="radio" name="q15_walls_fences_dummy" id="walls_fences_radio1" value="0"> No es relevante<!-- Not specified -->
				<br><input type="radio" name="q15_walls_fences_dummy" id="walls_fences_radio2" value="1"> No
				<br><input type="radio" name="q15_walls_fences_dummy" id="walls_fences_radio3" value="2"> S�, algunas se agrietaron<!-- Yes, some were cracked -->
				<br><input type="radio" name="q15_walls_fences_dummy" id="walls_fences_radio4" value="3"> S�, algunas cayeron parcialmente<!-- Yes, some partially fell -->
				<br><input type="radio" name="q15_walls_fences_dummy" id="walls_fences_radio5" value="4"> S�, algunas cayeron totalmente<!-- Yes, some fell completely -->
			</div>
			<br>
			<div class="building">
				<label>�Hubo alg�n da�o en el edificio? <!-- Was there any damage to the building? --></label>
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio1" value="0"> Sin da�os<!-- No Damage -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio2" value="5"> Grietas en las paredes<!-- Hairline cracks in walls -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio3" value="5"> Algunas grandes grietas en las paredes<!-- A few large cracks in walls -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio4" value="10"> Muchas grandes grietas en las paredes<!-- Many large cracks in walls -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio5" value="10"> Una o varias ventanas agrietadas<!-- More than one Cracks in windows -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio6" value="10"> Los azulejos del techo o los accesorios de iluminaci�n se cayeron<!-- Ceiling tiles or lighting fixtures fell -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio7" value="15"> Grietas en la escalera<!-- Cracks in stairs -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio8" value="15"> Muchas ventanas agrietadas o rotas<!-- Many windows cracked or some broken out -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio9" value="15"> El empa�ete se despeg� de las paredes<!-- Masonry fell from block or brick wall(s) -->
				<br><input class="building_class" type="radio" name="q16_buildings" id="building_radio10" value="15"> Pared exterior inclinada  o derrumbada<!-- Old chimney, major damage or fell down -->
			</div>
			<br>
			<div class="comment">
				<label>Commentarios adicionales<small>(Opcional)</small></label>
				<textarea class="form-control" name="user_comments" id="comment_textarea1" rows="3" placeholder="Escribe commentarios adicionales"></textarea>
			</div>
			<br>
			<label>Contacto <!-- Contact Information --><small>(Opcional)</small></label><br><br>
			<div class="name">
				<label> Nombre <!-- Name --></label>
				<input class="form-control" type="text"  name="user_name" id="name_text1" placeholder="Indicar tu nombre">
			</div>
			<br>
			<div class="email">
				<label> E-mail</label>
				<input class="form-control" type="text"  name="user_email" id="email_text1" placeholder="Indicar tu email">
			</div>
			<br>
			<div class="phone">
				<label> Tel�fono<!-- Phone --></label>
				<input class="form-control" type="text"  name="user_phone" id="phone_text1" placeholder="Indicar tu Tel�fono">
			</div>
			<br>
			<div align="center"><button type="" class="btn btn-primary btn-lg" onclick="check_before_submit()">Enviar</button></div>
		</form>
	</div>
</div>
</div>
<script>
window.onload = function what(){
	$('#Report_Datepicker').datepicker({
		format: 'yyyy-mm-dd',
		inline: true,
		container: '#datecon',
	});
	};
</script>
<%@ include file="footer.jsp" %>

