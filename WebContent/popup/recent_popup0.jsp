<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
    	String lat = request.getParameter("lat");
    	String lng = request.getParameter("lng");
    	String cdi = request.getParameter("cdi");
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABCUMBRfwwjAdfqWEQoT3ItITOAslxfec&language=es"></script>

<title>Insert title here</title>
</head>
<body>
<div id="map" style="width:625spx; height:430px" style="float:left;"></div>
<script>
	var myLatlng = new google.maps.LatLng(<% out.print(lat); %>, <% out.print(lng); %>);
	var myOptions = {
			  zoom: 10,
			  center: myLatlng
			};
	map = new google.maps.Map(document.getElementById("map"), myOptions);
	var myIcon = new google.maps.MarkerImage("/SGN_earthquake/static/image/marker_icon.png", null, null, null, new google.maps.Size(50,50));
	
	var markers = [];
	var marker = new google.maps.Marker({
	 	position: new google.maps.LatLng(<% out.print(lat); %>, <% out.print(lng); %>),
	    map: map,
	    title: 'Hello World!',
	    label:{
	    	text: "<% out.print(cdi); %>",
	    	color : "white",
	    	fontWeight: "bold",
	    	fontSize: "17px"
	    },
	    icon: myIcon
	});
	markers.push(marker);
	
</script>
</body>
</html>