<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
	<div class="custom2 est hidden-xs">
		<div class="container">

			<!-- contenido que se mostrara en secciones -->

			<!-- pie de pagina -->
			<div class="borderFooter"></div>
			<section class="footer">
				<div class="container">
					<div class="row hidden-xs">
						<div class="col-sm-4 col-sm-offset-4">
							<ul class="nav identidad">
								<!-- logo organismo para footer -->
								<li class="logo"><img src="./static/image/pie.jpg" width="67"
									height="67" class="img-responsive"
									alt="Servicio Geológico Nacional (SGN) de la República Dominicana es un "></li>
								<!-- separador -->
								<li class="separador"></li>
								<!-- identidad país para el footer -->
								<li class="rd"><img src="./static/image/repDomF.png" width="67"
									height="67" alt="República Dominicana"
									title="República Dominicana"></li>
							</ul>
						</div>
						<div class="col-sm-2 col-sm-offset-2 selloNortic">
							<!-- sello NORTIC -->
							<span></span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<p>
								<a class="nombreOrganismo" href="http://sgn.gob.do/">Servicio
									Geológico Nacional (SGN) de la República Dominicana es un </a>
							</p>
							<p class="hidden-xs">Ave. Winston Churchill # 75, Edificio
								"J. F. Martínez", 3er piso, Ensanche. Piantini, Santo Domingo,
								R.D.</p>
							<p class="hidden-xs">Tel.: 809-732-0363 | Fax: 809-689-2769</p>
							<p class="hidden-xs">
								<a href="mailto:info@sgn.gob.do?subject=Servicio%20Geol%C3%B3gico%20Nacional%20(SGN)%20de%20la%20Rep%C3%BAblica%20Dominicana%20es%20un%20%20-%20Contacto">info@sgn.gob.do</a>
							</p>

							<div class="moduletable hidden-xs  est hidden-xs">
								<div class="clrH3"></div>
								<div id="sUblevel1">
									<div id="sUblevel2">
										<div id="sUblevel3">
											<ul class="nav menu">
												<li class="item-130"><a
													href="http://sgn.gob.do/index.php/terminos-de-uso">Términos
														de uso</a></li>
												<li class="item-131"><a
													href="http://sgn.gob.do/index.php/politicas-de-privacidad">Políticas
														de privacidad</a></li>
												<li class="item-132"><a
													href="http://sgn.gob.do/index.php/preguntas-frecuentes">Preguntas
														frecuentes</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="moduletable visible-xs  est visible-xs">
								<div class="clrH3"></div>
								<div id="sUblevel1">
									<div id="sUblevel2">
										<div id="sUblevel3">
											<ul class="nav menu">
												<li class="item-103 active"><a
													href="http://sgn.gob.do/index.php">Inicio<span
														class="parentSign"></span></a></li>
												<li class="item-104"><a
													href="http://sgn.gob.do/index.php/mapa-de-sitio">Mapa
														de sitio</a></li>
												<li class="item-105"><a
													href="http://sgn.gob.do/index.php/contacto">Contacto</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<p></p>
							<p class="boton visible-xs">
								<a href="http://sgn.gob.do/#" class="subir"><span
									class="glyphicon glyphicon-download"></span>Subir</a>
							</p>
							<p class="visible-xs selloMobile"></p>
							<p>2017.Todos los derechos reservados</p>
							<p class="visible-xs">República Dominicana</p>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>	
</body>

</html>
