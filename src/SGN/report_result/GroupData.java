package SGN.report_result;

import java.sql.Timestamp;

public class GroupData {
	private double lat;
	private double lng;
	private Timestamp earthquake_reported_datetime;
	private double sum_times;
	private double count_times;
	private double sum_q5;
	private double count_q5;
	private double sum_q6;
	private double count_q6;
	private double sum_q8;
	private double count_q8;
	private double sum_q11;
	private double count_q11;
	private double sum_q12;
	private double count_q12;
	private double sum_q13;
	private double count_q13;
	private double sum_q16;
	private double count_q16;
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public Timestamp getEarthquake_reported_datetime() {
		return earthquake_reported_datetime;
	}
	public void setEarthquake_reported_datetime(Timestamp earthquake_reported_datetime) {
		this.earthquake_reported_datetime = earthquake_reported_datetime;
	}
	public double getSum_times() {
		return sum_times;
	}
	public void setSum_times(double sum_times) {
		this.sum_times = sum_times;
	}
	public double getCount_times() {
		return count_times;
	}
	public void setCount_times(double count_times) {
		this.count_times = count_times;
	}
	public double getSum_q5() {
		return sum_q5;
	}
	public void setSum_q5(double sum_q5) {
		this.sum_q5 = sum_q5;
	}
	public double getCount_q5() {
		return count_q5;
	}
	public void setCount_q5(double count_q5) {
		this.count_q5 = count_q5;
	}
	public double getSum_q6() {
		return sum_q6;
	}
	public void setSum_q6(double sum_q6) {
		this.sum_q6 = sum_q6;
	}
	public double getCount_q6() {
		return count_q6;
	}
	public void setCount_q6(double count_q6) {
		this.count_q6 = count_q6;
	}
	public double getSum_q8() {
		return sum_q8;
	}
	public void setSum_q8(double sum_q8) {
		this.sum_q8 = sum_q8;
	}
	public double getCount_q8() {
		return count_q8;
	}
	public void setCount_q8(double count_q8) {
		this.count_q8 = count_q8;
	}
	public double getSum_q11() {
		return sum_q11;
	}
	public void setSum_q11(double sum_q11) {
		this.sum_q11 = sum_q11;
	}
	public double getCount_q11() {
		return count_q11;
	}
	public void setCount_q11(double count_q11) {
		this.count_q11 = count_q11;
	}
	public double getSum_q12() {
		return sum_q12;
	}
	public void setSum_q12(double sum_q12) {
		this.sum_q12 = sum_q12;
	}
	public double getCount_q12() {
		return count_q12;
	}
	public void setCount_q12(double count_q12) {
		this.count_q12 = count_q12;
	}
	public double getSum_q13() {
		return sum_q13;
	}
	public void setSum_q13(double sum_q13) {
		this.sum_q13 = sum_q13;
	}
	public double getCount_q13() {
		return count_q13;
	}
	public void setCount_q13(double count_q13) {
		this.count_q13 = count_q13;
	}
	public double getSum_q16() {
		return sum_q16;
	}
	public void setSum_q16(double sum_q16) {
		this.sum_q16 = sum_q16;
	}
	public double getCount_q16() {
		return count_q16;
	}
	public void setCount_q16(double count_q16) {
		this.count_q16 = count_q16;
	}

}
