package SGN.report_result;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import SGN.report_result.DB_beans;

/**
 * Servlet implementation class Admin_Login_Servlet
 */
@WebServlet("/Admin_Login_Servlet")
public class Admin_Login_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin_Login_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try
		{
		     Admin_UserData user = new Admin_UserData();
		     DB_beans db_bean = new DB_beans();
		     user.setUsername(request.getParameter("username"));
		     user.setPassword(request.getParameter("password"));
		     user = db_bean.login(user);
		     
		     if (user.isValid())
		     {
		          HttpSession session = request.getSession(true);
		          session.setAttribute("currentSessionUser",user);
		          response.sendRedirect("./admin/admin_mainpage.jsp");
		     }
		     else {
		    	 response.sendRedirect("./admin/admin_login.jsp?status=loginfail");
		     }
		}
		catch (Throwable theException)
		{
		     System.out.println(theException); 
		}
	}
}

