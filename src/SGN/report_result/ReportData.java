package SGN.report_result;

import java.util.Date;

public class ReportData {
	private double id;
	private double lat;
	private double lng;
	private Date earthquake_reported_datetime;
	public String date;
	public String time;
	private Date submit_datetime;
	private double q1_feel;
	private double q4_others;
	private double q5_describe;
	private double q6_react;
	private double q8_difficult;
	private double q11_objects;
	private double q12_picture;
	private double q13_furniture;
	private double q16_buildings;
	private String q2_situation_dummy;
	private String q3_asleep_dummy;
	private String q7_respond_dummy;
	private String q9_swing_dummy;
	private String q10_hear_dummy;
	private String q14_heavy_appliance_dummy;
	private String q15_walls_fences_dummy;
	private String user_comments;
	private String user_name;
	private String user_email;
	private String user_phone;
	private String q2_situation_dummy_other_text;
	private String q7_respond_dummy_other_text;

	public double getId() {
		return id;
	}
	public void setId(double id) {
		this.id = id;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public Date getEarthquake_reported_datetime() {
		return earthquake_reported_datetime;
	}
	public void setEarthquake_reported_datetime(Date earthquake_reported_datetime) {
		this.earthquake_reported_datetime = earthquake_reported_datetime;
	}
	public Date getSubmit_datetime() {
		return submit_datetime;
	}
	public void setSubmit_datetime(Date submit_datetime) {
		this.submit_datetime = submit_datetime;
	}
	public double getQ1_feel() {
		return q1_feel;
	}
	public void setQ1_feel(double q1_feel) {
		this.q1_feel = q1_feel;
	}
	public double getQ4_others() {
		return q4_others;
	}
	public void setQ4_others(double q4_others) {
		this.q4_others = q4_others;
	}
	public double getQ5_describe() {
		return q5_describe;
	}
	public void setQ5_describe(double q5_describe) {
		this.q5_describe = q5_describe;
	}
	public double getQ6_react() {
		return q6_react;
	}
	public void setQ6_react(double q6_react) {
		this.q6_react = q6_react;
	}
	public double getQ8_difficult() {
		return q8_difficult;
	}
	public void setQ8_difficult(double q8_difficult) {
		this.q8_difficult = q8_difficult;
	}
	public double getQ11_objects() {
		return q11_objects;
	}
	public void setQ11_objects(double q11_objects) {
		this.q11_objects = q11_objects;
	}
	public double getQ12_picture() {
		return q12_picture;
	}
	public void setQ12_picture(double q12_picture) {
		this.q12_picture = q12_picture;
	}
	public double getQ13_furniture() {
		return q13_furniture;
	}
	public void setQ13_furniture(double q13_furniture) {
		this.q13_furniture = q13_furniture;
	}
	
	public double getQ16_buildings() {
		return q16_buildings;
	}
	public void setQ16_buildings(double q16_buildings) {
		this.q16_buildings = q16_buildings;
	}
	
	public String getQ2_situation_dummy() {
		return q2_situation_dummy;
	}
	public void setQ2_situation_dummy(String q2_situation_dummy) {
		this.q2_situation_dummy = q2_situation_dummy;
	}
	public String getQ3_asleep_dummy() {
		return q3_asleep_dummy;
	}
	public void setQ3_asleep_dummy(String q3_asleep_dummy) {
		this.q3_asleep_dummy = q3_asleep_dummy;
	}
	public String getQ7_respond_dummy() {
		return q7_respond_dummy;
	}
	public void setQ7_respond_dummy(String q7_respond_dummy) {
		this.q7_respond_dummy = q7_respond_dummy;
	}
	public String getQ9_swing_dummy() {
		return q9_swing_dummy;
	}
	public void setQ9_swing_dummy(String q9_swing_dummy) {
		this.q9_swing_dummy = q9_swing_dummy;
	}
	public String getQ10_hear_dummy() {
		return q10_hear_dummy;
	}
	public void setQ10_hear_dummy(String q10_hear_dummy) {
		this.q10_hear_dummy = q10_hear_dummy;
	}
	public String getQ14_heavy_appliance_dummy() {
		return q14_heavy_appliance_dummy;
	}
	public void setQ14_heavy_appliance_dummy(String q14_heavy_appliance_dummy) {
		this.q14_heavy_appliance_dummy = q14_heavy_appliance_dummy;
	}
	public String getQ15_walls_fences_dummy() {
		return q15_walls_fences_dummy;
	}
	public void setQ15_walls_fences_dummy(String q15_walls_fences_dummy) {
		this.q15_walls_fences_dummy = q15_walls_fences_dummy;
	}
	public String getUser_comments() {
		return user_comments;
	}
	public void setUser_comments(String user_comments) {
		this.user_comments = user_comments;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getQ2_situation_dummy_other_text() {
		return q2_situation_dummy_other_text;
	}
	public void setQ2_situation_dummy_other_text(String q2_situation_dummy_other_text) {
		this.q2_situation_dummy_other_text = q2_situation_dummy_other_text;
	}
	public String getQ7_respond_dummy_other_text() {
		return q7_respond_dummy_other_text;
	}
	public void setQ7_respond_dummy_other_text(String q7_respond_dummy_other_text) {
		this.q7_respond_dummy_other_text = q7_respond_dummy_other_text;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
}
