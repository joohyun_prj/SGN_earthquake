package SGN.report_result;

import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class DB_beans {
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	
	String jdbc_driver = "com.mysql.jdbc.Driver"; // string for jdbc driver load.
	String jdbc_url = "jdbc:mysql://localhost/jspdb"; // database information.
	
	
	// connect function is used to connect DB with JDBC driver.
	void connect() {
		try {
			Class.forName(jdbc_driver); // load jdbc driver
			conn = DriverManager.getConnection(jdbc_url,"root","rootpassword"); // db id and password
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// disconnect function is used to save resource.
	void disconnect() {
		try {
			pstmt.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		if(conn!=null) {
			try {
				conn.close();
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	// get_refresh_time is used to get last refresh time that is needed for comparison with now time.
	public java.sql.Timestamp get_refresh_time(){
		connect();
		String sql = "select last_refresh_time from SGN_earthquake_last_refresh_time";
		java.sql.Timestamp refresh_time = java.sql.Timestamp.valueOf("2017-01-01 00:00:00");

		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rtn = pstmt.executeQuery();
			while(rtn.next()) {
				refresh_time = rtn.getTimestamp("last_refresh_time");
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			disconnect();
		}
		return refresh_time;
	}
	
	// set_refresh_time is to update refresh_time in 'SGN_earthquake_last_refresh_time' table
	public void set_refresh_time() {
		
		connect();
		String sql = "update SGN_earthquake_last_refresh_time set last_refresh_time=?";
		java.sql.Timestamp now_time = new Timestamp(new Date().getTime());
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setTimestamp(1,now_time);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			disconnect();
		}
	}
	
	public boolean insertDB(ReportData reportdata) {

		connect();

		String sql = "insert into sgn_earthquake_report(id,lat,lng,earthquake_reported_datetime,submit_datetime,q1,q2_dummy,q3_dummy,"
				+ "q4,q5,q6,q7_dummy,q8,q9_dummy,q10_dummy,q11,q12,q13,q14_dummy,q15_dummy,q16,user_comments,"
				+ "user_name,user_email,user_phone) values(0,?,?,?,now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1,Math.round(reportdata.getLat()*100)/100.0);
			pstmt.setDouble(2,Math.round(reportdata.getLng()*100)/100.0);

			String a = reportdata.getDate() + " " + reportdata.getTime();

			pstmt.setTimestamp(3,java.sql.Timestamp.valueOf(a));
			pstmt.setDouble(4,reportdata.getQ1_feel());
			if(!reportdata.getQ2_situation_dummy().equals("5")) {
				pstmt.setString(5,reportdata.getQ2_situation_dummy());
			}
			else {
				pstmt.setString(5,reportdata.getQ2_situation_dummy_other_text());
			}
			pstmt.setString(6,reportdata.getQ3_asleep_dummy());
//			###############	pstmt.setDouble(6,q4_others_feel);
			if (reportdata.getQ4_others()==99) {
				pstmt.setNull(7,java.sql.Types.INTEGER);		

			}
			else
				pstmt.setDouble(7,reportdata.getQ4_others());

//			###############		pstmt.setDouble(7,q5_describe);
			if (reportdata.getQ5_describe()==99)
				pstmt.setNull(8,java.sql.Types.INTEGER);			
			else
				pstmt.setDouble(8,reportdata.getQ5_describe());

//			###############		pstmt.setDouble(8,q6_react);
			if (reportdata.getQ6_react()==99)
				pstmt.setNull(9,java.sql.Types.INTEGER);			
			else
				pstmt.setDouble(9,reportdata.getQ6_react());		
			
//			###############		pstmt.setDouble(9,q7_respond_dummy);

			if(!reportdata.getQ7_respond_dummy().equals("5")) {
				pstmt.setString(10,reportdata.getQ7_respond_dummy());	
			}
			else {
				pstmt.setString(10,reportdata.getQ7_respond_dummy_other_text());
			}
			
			//pstmt.setDouble(10,0);		
//			###############		pstmt.setDouble(10,q8_difficult);
			if (reportdata.getQ8_difficult()==99)
				pstmt.setNull(11,java.sql.Types.INTEGER);			
			else
				pstmt.setDouble(11,reportdata.getQ8_difficult());	
			
			pstmt.setString(12,reportdata.getQ9_swing_dummy());
			pstmt.setString(13,reportdata.getQ10_hear_dummy());
			
//			###############		pstmt.setDouble(13,q11_objects);
			if (reportdata.getQ11_objects()==99)
				pstmt.setNull(14,java.sql.Types.INTEGER);			
			else
				pstmt.setDouble(14,reportdata.getQ11_objects());	
			
//			###############		pstmt.setDouble(14,q12_picture);
			if (reportdata.getQ12_picture()==99)
				pstmt.setNull(15,java.sql.Types.INTEGER);			
			else
				pstmt.setDouble(15,reportdata.getQ12_picture());		
			
//			###############		pstmt.setDouble(15,q13_furniture);
			if (reportdata.getQ13_furniture()==99)
				pstmt.setNull(16,java.sql.Types.INTEGER);			
			else
				pstmt.setDouble(16,reportdata.getQ13_furniture());
			
//			###############		pstmt.setDouble(15,q13_furniture);

			pstmt.setString(17,reportdata.getQ14_heavy_appliance_dummy());
			pstmt.setString(18,reportdata.getQ15_walls_fences_dummy());
			pstmt.setDouble(19,reportdata.getQ16_buildings());

			pstmt.setString(20,reportdata.getUser_comments());
			pstmt.setString(21,reportdata.getUser_name());
			pstmt.setString(22,reportdata.getUser_email());
			pstmt.setString(23,reportdata.getUser_phone());	
			pstmt.executeUpdate();
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		finally {
			disconnect();
		}
		return true;
	}
	
	// index_insertDB function is used to insert computed data to DB. this data is computed data.
	public boolean index_insertDB(GroupData groupdata) {
		connect();
		admin admin = new admin();
		IndexData rtn = admin.Mid_compute_cdi(groupdata);
		String sql = "insert into index_sgn_earthquake_report"
				+ " values (0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try{
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1,groupdata.getLat()); // IndexData that Mid_compute_cdi function return doesn't have lat and lng and reproted_time.
			pstmt.setDouble(2,groupdata.getLng()); // so about these three values, we use groupdata that is input data
			pstmt.setTimestamp(3,groupdata.getEarthquake_reported_datetime()); // because Mid_compute_cdi don't use lat, lng and time.
			pstmt.setDouble(4,rtn.getIndex_sum_times());
			pstmt.setDouble(5,rtn.getIndex_count_times());
			pstmt.setDouble(6,rtn.getIndex_sum_q5());
			pstmt.setDouble(7,rtn.getIndex_count_q5());
			pstmt.setDouble(8,rtn.getIndex_sum_q6());
			pstmt.setDouble(9,rtn.getIndex_count_q6());
			pstmt.setDouble(10,rtn.getIndex_sum_q8());
			pstmt.setDouble(11,rtn.getIndex_count_q8());
			pstmt.setDouble(12,rtn.getIndex_sum_q11());
			pstmt.setDouble(13,rtn.getIndex_count_q11());
			pstmt.setDouble(14,rtn.getIndex_sum_q12());	 
			pstmt.setDouble(15,rtn.getIndex_count_q12());
			pstmt.setDouble(16,rtn.getIndex_sum_q13());
			pstmt.setDouble(17,rtn.getIndex_count_q13());
			pstmt.setDouble(18,rtn.getIndex_sum_q16());
			pstmt.setDouble(19,rtn.getIndex_count_q16());
			
			pstmt.setDouble(20,rtn.getIndex_cws());
			pstmt.setDouble(21,rtn.getIndex_cdi());

			pstmt.executeUpdate();
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		finally {
			disconnect();
		}
		return true;
	}

	// getGroupDB function is used to execute query that has group_by clause. and return value getGroupDB returns is used for CDI value.
	public ArrayList<GroupData> getgroupDB(java.sql.Timestamp updated_time){
			connect();
			ArrayList<GroupData> array_groupdata = new ArrayList<GroupData>();
			String sql = "select lat,lng,earthquake_reported_datetime,"
					+ "sum(q1_times_q4) as sum_times,count(q1_times_q4) as count_times,"
					+ "sum(q5) as sum_q5,count(q5) as count_q5,sum(q6) as sum_q6,count(q6) as count_q6,"
					+ "sum(q8) as sum_q8,count(q8) as count_q8,sum(q11) as sum_q11,count(q11) as count_q11,"
					+ "sum(q12) as sum_q12,count(q12) as count_q12,sum(q13) as sum_q13,count(q13) as count_q13,"
					+ "sum(q16) as sum_q16,count(q16) as count_q16 "
					+ "from sgn_earthquake_report where submit_datetime > ? "
					+ "group by lat,lng,earthquake_reported_datetime";
			try {
				pstmt = conn.prepareStatement(sql);
				pstmt.setTimestamp(1,updated_time);
				ResultSet rnt = pstmt.executeQuery();
				while(rnt.next()) {
					GroupData groupdata = new GroupData();
					
					groupdata.setLat(rnt.getDouble("lat"));
					groupdata.setLng(rnt.getDouble("lng"));
					groupdata.setEarthquake_reported_datetime(rnt.getTimestamp("earthquake_reported_datetime"));
					groupdata.setSum_times(rnt.getDouble("sum_times"));
					groupdata.setCount_times(rnt.getDouble("count_times"));
					groupdata.setSum_q5(rnt.getDouble("sum_q5"));
					groupdata.setCount_q5(rnt.getDouble("count_q5"));
					groupdata.setSum_q6(rnt.getDouble("sum_q6"));
					groupdata.setCount_q6(rnt.getDouble("count_q6"));
					groupdata.setSum_q8(rnt.getDouble("sum_q8"));
					groupdata.setCount_q8(rnt.getDouble("count_q8"));
					groupdata.setSum_q11(rnt.getDouble("sum_q11"));
					groupdata.setCount_q11(rnt.getDouble("count_q11"));
					groupdata.setSum_q12(rnt.getDouble("sum_q12"));
					groupdata.setCount_q12(rnt.getDouble("count_q12"));
					groupdata.setSum_q13(rnt.getDouble("sum_q13"));
					groupdata.setCount_q13(rnt.getDouble("count_q13"));
					groupdata.setSum_q16(rnt.getDouble("sum_q16"));
					groupdata.setCount_q16(rnt.getDouble("count_q16"));
					array_groupdata.add(groupdata);
				}
				rnt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} 
			finally {
				disconnect();
			}
			return array_groupdata;
	}

	public ArrayList<ReportData> getReportTable(java.sql.Timestamp from_filter_time, java.sql.Timestamp to_filter_time){
		connect();
		ArrayList<ReportData> array_reportdata = new ArrayList<ReportData>();
		String sql = "select * from sgn_earthquake_report where submit_datetime between ? and ? "
				+ "order by submit_datetime desc,id desc,lat,lng,user_name,user_email";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setTimestamp(1,from_filter_time);
			pstmt.setTimestamp(2,to_filter_time);
			ResultSet rnt = pstmt.executeQuery();
			while(rnt.next()) {
				ReportData reportdata = new ReportData();
				reportdata.setId(rnt.getDouble("id"));
				reportdata.setLat(rnt.getDouble("lat"));
				reportdata.setLng(rnt.getDouble("lng"));
				reportdata.setEarthquake_reported_datetime(rnt.getTimestamp("earthquake_reported_datetime"));
				reportdata.setSubmit_datetime(rnt.getTimestamp("submit_datetime"));
				reportdata.setQ1_feel(rnt.getDouble("q1"));
				reportdata.setQ2_situation_dummy(rnt.getString("q2_dummy"));
				reportdata.setQ3_asleep_dummy(rnt.getString("q3_dummy"));
				reportdata.setQ4_others(rnt.getDouble("q4"));
				reportdata.setQ5_describe(rnt.getDouble("q5"));
				reportdata.setQ6_react(rnt.getDouble("q6"));
				reportdata.setQ7_respond_dummy(rnt.getString("q7_dummy"));
				reportdata.setQ8_difficult(rnt.getDouble("q8"));
				reportdata.setQ9_swing_dummy(rnt.getString("q9_dummy"));
				reportdata.setQ10_hear_dummy(rnt.getString("q10_dummy"));
				reportdata.setQ11_objects(rnt.getDouble("q11"));
				reportdata.setQ12_picture(rnt.getDouble("q12"));
				reportdata.setQ13_furniture(rnt.getDouble("q13"));
				reportdata.setQ14_heavy_appliance_dummy(rnt.getString("q14_dummy"));
				reportdata.setQ15_walls_fences_dummy(rnt.getString("q15_dummy"));
				reportdata.setQ16_buildings(rnt.getDouble("q16"));
				reportdata.setUser_comments(rnt.getString("user_comments"));
				reportdata.setUser_name(rnt.getString("user_name"));
				reportdata.setUser_email(rnt.getString("user_email"));
				reportdata.setUser_phone(rnt.getString("user_phone"));
				array_reportdata.add(reportdata);
			}
			rnt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return array_reportdata;
	}
	
	public ArrayList<ReportData> getReportTable(){
		connect();
		ArrayList<ReportData> array_reportdata = new ArrayList<ReportData>();
		String sql = "select * from sgn_earthquake_report order by submit_datetime desc,id desc,lat,lng,user_name,user_email";
		try {
			pstmt = conn.prepareStatement(sql);
			//pstmt.setTimestamp(1,updated_time);
			ResultSet rnt = pstmt.executeQuery();
			while(rnt.next()) {
				ReportData reportdata = new ReportData();
				reportdata.setId(rnt.getDouble("id"));
				reportdata.setLat(rnt.getDouble("lat"));
				reportdata.setLng(rnt.getDouble("lng"));
				reportdata.setEarthquake_reported_datetime(rnt.getTimestamp("earthquake_reported_datetime"));
				reportdata.setSubmit_datetime(rnt.getTimestamp("submit_datetime"));
				reportdata.setQ1_feel(rnt.getDouble("q1"));
				reportdata.setQ2_situation_dummy(rnt.getString("q2_dummy"));
				reportdata.setQ3_asleep_dummy(rnt.getString("q3_dummy"));
				reportdata.setQ4_others(rnt.getDouble("q4"));
				reportdata.setQ5_describe(rnt.getDouble("q5"));
				reportdata.setQ6_react(rnt.getDouble("q6"));
				reportdata.setQ7_respond_dummy(rnt.getString("q7_dummy"));
				reportdata.setQ8_difficult(rnt.getDouble("q8"));
				reportdata.setQ9_swing_dummy(rnt.getString("q9_dummy"));
				reportdata.setQ10_hear_dummy(rnt.getString("q10_dummy"));
				reportdata.setQ11_objects(rnt.getDouble("q11"));
				reportdata.setQ12_picture(rnt.getDouble("q12"));
				reportdata.setQ13_furniture(rnt.getDouble("q13"));
				reportdata.setQ14_heavy_appliance_dummy(rnt.getString("q14_dummy"));
				reportdata.setQ15_walls_fences_dummy(rnt.getString("q15_dummy"));
				reportdata.setQ16_buildings(rnt.getDouble("q16"));
				reportdata.setUser_comments(rnt.getString("user_comments"));
				reportdata.setUser_name(rnt.getString("user_name"));
				reportdata.setUser_email(rnt.getString("user_email"));
				reportdata.setUser_phone(rnt.getString("user_phone"));
				array_reportdata.add(reportdata);
			}
			rnt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return array_reportdata;
	}
	

	// getindexDB is used to get data from index table. returned value is used for CDI computation
	public ArrayList<IndexData> getIndexTable(java.sql.Timestamp from_filter_time, java.sql.Timestamp to_filter_time){
		connect();
		ArrayList<IndexData> array_indexdata = new ArrayList<IndexData>();
		String sql = "select * from index_sgn_earthquake_report where index_earthquake_reported_datetime between ? and ? "
				+ "order by index_earthquake_reported_datetime desc,id desc,index_lat,index_lng";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setTimestamp(1,from_filter_time);
			pstmt.setTimestamp(2,to_filter_time);
			ResultSet rnt = pstmt.executeQuery();
			while(rnt.next()) {
				IndexData indexdata = new IndexData();
				indexdata.setIndex_id(rnt.getDouble("id"));
				indexdata.setIndex_lat(rnt.getDouble("index_lat"));
				indexdata.setIndex_lng(rnt.getDouble("index_lng"));
				indexdata.setIndex_earthquake_reported_datetime(rnt.getTimestamp("index_earthquake_reported_datetime"));
				indexdata.setIndex_sum_times(rnt.getDouble("index_sum_times"));
				indexdata.setIndex_count_times(rnt.getDouble("index_count_times"));
				indexdata.setIndex_sum_q5(rnt.getDouble("index_sum_q5"));
				indexdata.setIndex_count_q5(rnt.getDouble("index_count_q5"));
				indexdata.setIndex_sum_q6(rnt.getDouble("index_sum_q6"));
				indexdata.setIndex_count_q6(rnt.getDouble("index_count_q6"));
				indexdata.setIndex_sum_q8(rnt.getDouble("index_sum_q8"));
				indexdata.setIndex_count_q8(rnt.getDouble("index_count_q8"));
				indexdata.setIndex_sum_q11(rnt.getDouble("index_sum_q11"));
				indexdata.setIndex_count_q11(rnt.getDouble("index_count_q11"));
				indexdata.setIndex_sum_q12(rnt.getDouble("index_sum_q12"));
				indexdata.setIndex_count_q12(rnt.getDouble("index_count_q12"));
				indexdata.setIndex_sum_q13(rnt.getDouble("index_sum_q13"));
				indexdata.setIndex_count_q13(rnt.getDouble("index_count_q13"));
				indexdata.setIndex_sum_q16(rnt.getDouble("index_sum_q16"));
				indexdata.setIndex_count_q16(rnt.getDouble("index_count_q16"));
				indexdata.setIndex_cws(rnt.getDouble("index_cws"));
				indexdata.setIndex_cdi(rnt.getDouble("index_cdi"));
				array_indexdata.add(indexdata);
			}
			rnt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return array_indexdata;
	}
	
	public ArrayList<IndexData> getIndexTable(){
		connect();
		ArrayList<IndexData> array_indexdata = new ArrayList<IndexData>();
		String sql = "select * from index_sgn_earthquake_report order by index_earthquake_reported_datetime desc,id desc,index_lat,index_lng";
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rnt = pstmt.executeQuery();
			while(rnt.next()) {
				IndexData indexdata = new IndexData();
				indexdata.setIndex_id(rnt.getDouble("id"));
				indexdata.setIndex_lat(rnt.getDouble("index_lat"));
				indexdata.setIndex_lng(rnt.getDouble("index_lng"));
				indexdata.setIndex_earthquake_reported_datetime(rnt.getTimestamp("index_earthquake_reported_datetime"));
				indexdata.setIndex_sum_times(rnt.getDouble("index_sum_times"));
				indexdata.setIndex_count_times(rnt.getDouble("index_count_times"));
				indexdata.setIndex_sum_q5(rnt.getDouble("index_sum_q5"));
				indexdata.setIndex_count_q5(rnt.getDouble("index_count_q5"));
				indexdata.setIndex_sum_q6(rnt.getDouble("index_sum_q6"));
				indexdata.setIndex_count_q6(rnt.getDouble("index_count_q6"));
				indexdata.setIndex_sum_q8(rnt.getDouble("index_sum_q8"));
				indexdata.setIndex_count_q8(rnt.getDouble("index_count_q8"));
				indexdata.setIndex_sum_q11(rnt.getDouble("index_sum_q11"));
				indexdata.setIndex_count_q11(rnt.getDouble("index_count_q11"));
				indexdata.setIndex_sum_q12(rnt.getDouble("index_sum_q12"));
				indexdata.setIndex_count_q12(rnt.getDouble("index_count_q12"));
				indexdata.setIndex_sum_q13(rnt.getDouble("index_sum_q13"));
				indexdata.setIndex_count_q13(rnt.getDouble("index_count_q13"));
				indexdata.setIndex_sum_q16(rnt.getDouble("index_sum_q16"));
				indexdata.setIndex_count_q16(rnt.getDouble("index_count_q16"));
				indexdata.setIndex_cws(rnt.getDouble("index_cws"));
				indexdata.setIndex_cdi(rnt.getDouble("index_cdi"));
				array_indexdata.add(indexdata);
			}
			rnt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return array_indexdata;
	}

	// getindexDB is used to get data from index table. returned value is used for CDI computation
	public IndexData getindexDB(double lat, double lng, java.sql.Timestamp reported_time){
		connect();
		String sql = "select * from index_sgn_earthquake_report where index_lat=? and index_lng = ? and index_earthquake_reported_datetime = ?";
		IndexData indexdata = new IndexData();
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1,lat);
			pstmt.setDouble(2,lng);
			pstmt.setTimestamp(3,reported_time);
			
			ResultSet rtn = pstmt.executeQuery();
			while(rtn.next()) {
				
				indexdata.setIndex_lat(rtn.getDouble("index_lat"));
				indexdata.setIndex_lng(rtn.getDouble("index_lng"));
				indexdata.setIndex_earthquake_reported_datetime(rtn.getTimestamp("index_earthquake_reported_datetime"));
				indexdata.setIndex_sum_times(rtn.getDouble("index_sum_times"));
				indexdata.setIndex_count_times(rtn.getDouble("index_count_times"));
				indexdata.setIndex_sum_q5(rtn.getDouble("index_sum_q5"));
				indexdata.setIndex_count_q5(rtn.getDouble("index_count_q5"));
				indexdata.setIndex_sum_q6(rtn.getDouble("index_sum_q6"));
				indexdata.setIndex_count_q6(rtn.getDouble("index_count_q6"));
				indexdata.setIndex_sum_q8(rtn.getDouble("index_sum_q8"));
				indexdata.setIndex_count_q8(rtn.getDouble("index_count_q8"));
				indexdata.setIndex_sum_q11(rtn.getDouble("index_sum_q11"));
				indexdata.setIndex_count_q11(rtn.getDouble("index_count_q11"));
				indexdata.setIndex_sum_q12(rtn.getDouble("index_sum_q12"));
				indexdata.setIndex_count_q12(rtn.getDouble("index_count_q12"));
				indexdata.setIndex_sum_q13(rtn.getDouble("index_sum_q13"));
				indexdata.setIndex_count_q13(rtn.getDouble("index_count_q13"));
				indexdata.setIndex_sum_q16(rtn.getDouble("index_sum_q16"));
				indexdata.setIndex_count_q16(rtn.getDouble("index_count_q16"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return indexdata;
		}
	
	// check_db function is used to be used whether there is data in index table or not.
	public boolean check_db(double lat, double lng, java.sql.Timestamp reported_time) {
		connect();
		String sql = "select id from index_sgn_earthquake_report where index_lat=? and index_lng = ? and index_earthquake_reported_datetime = ?";
		boolean final_rtn=false;
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1,lat);
			pstmt.setDouble(2,lng);
			pstmt.setTimestamp(3,reported_time);
			ResultSet rtn = pstmt.executeQuery();
			ArrayList<Integer> id = new ArrayList<Integer>();
			
			while(rtn.next()) {
				id.add(rtn.getInt("id"));
			}
			if(id.size() != 0) {
				final_rtn = true;
			}
			else{
				final_rtn = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return final_rtn;
	}

	// update_index_db function is used to update index_table.
	public boolean update_index_db(double lat, double lng, java.sql.Timestamp reported_time,IndexData updatedata) {
		connect();
		String sql = "update index_sgn_earthquake_report" + 
				" set index_sum_times=?, index_count_times=?," + 
				"index_sum_q5=?, index_count_q5=?," + 
				"index_sum_q6=?, index_count_q6=?," + 
				"index_sum_q8=?, index_count_q8=?," + 
				"index_sum_q11=?, index_count_q11=?," + 
				"index_sum_q12=?, index_count_q12=?," + 
				"index_sum_q13=?, index_count_q13=?," + 
				"index_sum_q16=?, index_count_q16=?" + 
				"where index_lat=? and index_lng = ? and index_earthquake_reported_datetime = ?";
		boolean final_rtn=false;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setDouble(1,updatedata.getIndex_sum_times());
			pstmt.setDouble(2,updatedata.getIndex_count_times());
			pstmt.setDouble(3,updatedata.getIndex_sum_q5());
			pstmt.setDouble(4,updatedata.getIndex_count_q5());
			pstmt.setDouble(5,updatedata.getIndex_sum_q6());
			pstmt.setDouble(6,updatedata.getIndex_count_q6());
			pstmt.setDouble(7,updatedata.getIndex_sum_q8());
			pstmt.setDouble(8,updatedata.getIndex_count_q8());
			pstmt.setDouble(9,updatedata.getIndex_sum_q11());
			pstmt.setDouble(10,updatedata.getIndex_count_q11());
			pstmt.setDouble(11,updatedata.getIndex_sum_q12());
			pstmt.setDouble(12,updatedata.getIndex_count_q12());
			pstmt.setDouble(13,updatedata.getIndex_sum_q13());
			pstmt.setDouble(14,updatedata.getIndex_count_q13());
			pstmt.setDouble(15,updatedata.getIndex_sum_q16());
			pstmt.setDouble(16,updatedata.getIndex_count_q16());
			
			pstmt.setDouble(17,lat);
			pstmt.setDouble(18,lng);
			pstmt.setTimestamp(19,reported_time);
			pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		
		return final_rtn;
	}
		
	// getIndexDBwithFilter function is used to filter index table rows by month and year that selected by user.
	public ArrayList<IndexData> getIndexDBwithFilter(int month, int year){
		connect();
				
		int final_month = month;
		int final_year = year;
		ArrayList<IndexData> array_indexdata = new ArrayList<IndexData>();
		String sql = "select index_lat,index_lng,index_cdi from index_sgn_earthquake_report where month(index_earthquake_reported_datetime)=? "
				+ "and year(index_earthquake_reported_datetime)=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, final_month);
			pstmt.setInt(2, final_year);

			ResultSet rtn = pstmt.executeQuery();
			while(rtn.next()) {
				IndexData indexdata = new IndexData();
				
				indexdata.setIndex_lat(rtn.getDouble("index_lat"));
				indexdata.setIndex_lng(rtn.getDouble("index_lng"));
				indexdata.setIndex_cdi(rtn.getDouble("index_cdi"));
				array_indexdata.add(indexdata);
			}
			rtn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return array_indexdata;
		}
	
	// GetRecentReport gets recent five data(rows) of sgn_earthquake_report table.
	public ArrayList<RecentData> GetRecentReport(){
		connect();
				
		ArrayList<RecentData> array_recentdata = new ArrayList<RecentData>();
		String sql = "select lat,lng,earthquake_reported_datetime,q1,final_q4,q5,q6,q8,q11,q12,q13,q16 "
				+ "from sgn_earthquake_report order by submit_datetime DESC limit 5";
		try {
			pstmt = conn.prepareStatement(sql);

			ResultSet rtn = pstmt.executeQuery();
			while(rtn.next()) {
				
				ComputeCdiData computecdidata = new ComputeCdiData();
				
				double lat = rtn.getDouble("lat");
				double lng = rtn.getDouble("lng");
				Timestamp report_datetime = rtn.getTimestamp("earthquake_reported_datetime");
				computecdidata.setQ1(rtn.getDouble("q1"));
				computecdidata.setFinal_q4(rtn.getDouble("final_q4"));
				computecdidata.setQ5(rtn.getDouble("q5"));
				computecdidata.setQ6(rtn.getDouble("q6"));
				computecdidata.setQ8(rtn.getDouble("q8"));
				computecdidata.setQ11(rtn.getDouble("q11"));
				computecdidata.setQ12(rtn.getDouble("q12"));
				computecdidata.setQ13(rtn.getDouble("q13"));
				computecdidata.setQ16(rtn.getDouble("q16"));

				// compute cdi using reportdata. then rerutn set to recentdata object and then return recentdata
				
				admin admin = new admin();
				double cdi = admin.Mid_compute_cdi(computecdidata);				
				RecentData recentdata = new RecentData();
				
				recentdata.setLat(lat);
				recentdata.setLng(lng);
				recentdata.setReport_datetime(report_datetime);
				recentdata.setCdi(cdi);
				
				array_recentdata.add(recentdata);
			}
			rtn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			disconnect();
		}
		return array_recentdata;
		}
	
	public boolean reset_indextable() {
		boolean rtn=false;
		connect();
		
		String sql = "truncate index_sgn_earthquake_report";
		String sql2 = "update SGN_earthquake_last_refresh_time set last_refresh_time = '0000-00-00 01:00:00'";

		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			pstmt2.executeUpdate();
			
			if(synchronize_db()) {
				rtn = true;
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			disconnect();
		}
		return rtn;
	}

	public boolean synchronize_db() {
		admin admin = new admin();
		ArrayList<GroupData> gd = new ArrayList<GroupData>();
		gd = getgroupDB(get_refresh_time()); // groupdata array list that after submit_test
		int i;

		for (i=0;i<gd.size();i++){
			if(check_db(gd.get(i).getLat(),gd.get(i).getLng(),gd.get(i).getEarthquake_reported_datetime())){ 
				// check whether there is some row that has same lat, lng and time or not, so if there is row, execute update query 
				IndexData update_set = new IndexData();
				IndexData former_set = new IndexData();
				
				former_set = getindexDB(gd.get(i).getLat(),gd.get(i).getLng(),gd.get(i).getEarthquake_reported_datetime());
				// former_set is literally before update data set. so when we compute CDI, we will use former set because of efficiency.
				
				update_set = admin.Mid_compute_cdi(gd.get(i), former_set);
				// Mid_compute_cdi function compute CDI then return sesult IndexData arraylist.
				update_index_db(gd.get(i).getLat(),gd.get(i).getLng(),gd.get(i).getEarthquake_reported_datetime(),update_set);
				// and next, we update it to database.
			}
			else{
				// check whether there is some row that has same lat, lng and reported_time or not, so if there is not row, execute insert query
				if(index_insertDB(gd.get(i))){ // check whether insert is valid or not.
					System.out.println("index insert success~!");
				}
				else{
					System.out.println("index insert fail~!");
					return false;
				}
			}			
		}
		set_refresh_time();
		return true;
	}
	
	//****READ ME Before executing CSV export function****
	//mysql allow us to export csv file only to limited directory
	//in my case, exportable directory is set to C:\ProgramData\MySQL\MySQL Server 5.7\Uploads as a default
	//you must check your exportable directory by executing sql query "show variables like 'secure%';" on the mysql workbench
	//after check exportable directory, change sql query after into outfile command to your own exportable directory
	//also you can add more directory to temporarily export csv file by changing my.ini(windows os) or my.cnf file(linux os)
	//more specifically, search for secure-file-priv="C:/ProgramData/MySQL/MySQL Server 5.7/Uploads" and add more directory below
	String mysql_exportable_directory = "C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/";
	//in my case, exportable directory is set to C:\ProgramData\MySQL\MySQL Server 5.7\Uploads as a default
	//after check your exportable directory, change sql query after into outfile command to your own exportable directory

	
	public boolean exportReportCSV(String final_filepath_string, String filename){
		connect();
		String sql = "select 'id', 'lat', 'lng', 'earthquake_reported_datetime', 'submit_datetime', 'q1', 'q2_dummy', 'q3_dummy', 'q4', 'final_q4', 'q1_time_q4', 'q5', 'q6', 'q7_dummy', 'q8', 'q9_dummy', 'q10_dummy', 'q11', 'q12', 'q13', 'q14_dummy', 'q15_dummy', 'q16', 'user_comments', 'user_name', 'user_email', 'user_phone' "
				+ "union all "
				+ "select * from sgn_earthquake_report "
				+ "order by submit_datetime desc, id desc, lat, lng "
				+ "INTO OUTFILE '"+ mysql_exportable_directory + filename + ".csv' "
				+ "FIELDS ENCLOSED BY '\"'" 
				+ "TERMINATED BY ';'" 
				+ "ESCAPED BY '\"'" 
				+ "LINES TERMINATED BY '\r\n';";
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rtn = pstmt.executeQuery();
			//temporarily save csv file at the mysql_exportable_directory
			
			String temp_path_file_string = mysql_exportable_directory+filename+".csv";
			
			String final_filepath_string_temp = final_filepath_string.replace("\\", "/");
			if(final_filepath_string_temp.endsWith("/")) {
				final_filepath_string=final_filepath_string_temp.substring(1, final_filepath_string_temp.length()-1);
			}
			else {
				final_filepath_string=final_filepath_string_temp;
			}
			
			File final_filepath = new File(final_filepath_string);
			if (!final_filepath.exists()) { //if there is no such filepath, make new directory
				final_filepath.mkdirs();
	        }

			String final_path_file_string = final_filepath_string+"/"+filename+".csv";

			File temp_path_file = new File(temp_path_file_string);
		    File final_path_file = new File(final_path_file_string);

		    if(temp_path_file.exists())
		    {
		    	temp_path_file.renameTo(final_path_file);
		    	if(!final_path_file.exists()) {
		    		System.out.println("java file move fail");
		    	}
		    	else {
					System.out.println("csv export success");
		    	}
		    }
		    else {
		    	System.out.println("mysql export fail");
		    }
			//rtn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} 
		finally {
			disconnect();
		}
		return true;
	}
	
	public boolean exportReportCSV(String final_filepath_string, String filename, Timestamp from_datetime, Timestamp to_datetime){
		connect();
		String sql = "select 'id', 'lat', 'lng', 'earthquake_reported_datetime', 'submit_datetime', 'q1', 'q2_dummy', 'q3_dummy', 'q4', 'final_q4', 'q1_time_q4', 'q5', 'q6', 'q7_dummy', 'q8', 'q9_dummy', 'q10_dummy', 'q11', 'q12', 'q13', 'q14_dummy', 'q15_dummy', 'q16', 'user_comments', 'user_name', 'user_email', 'user_phone' "
				+ "union all "
				+ "select * from sgn_earthquake_report "
				+ "where submit_datetime between ? and ? "
				+ "order by submit_datetime desc, id desc, lat, lng "
				+ "INTO OUTFILE '"+ mysql_exportable_directory + filename + ".csv' "
				+ "FIELDS ENCLOSED BY '\"'" 
				+ "TERMINATED BY ';'" 
				+ "ESCAPED BY '\"'" 
				+ "LINES TERMINATED BY '\r\n';";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setTimestamp(1,from_datetime);
			pstmt.setTimestamp(2,to_datetime);
			ResultSet rtn = pstmt.executeQuery();
			//temporarily save csv file at the mysql_exportable_directory
			
			String temp_path_file_string = mysql_exportable_directory+filename+".csv";
			
			String final_filepath_string_temp = final_filepath_string.replace("\\", "/");
			if(final_filepath_string_temp.endsWith("/")) {
				final_filepath_string=final_filepath_string_temp.substring(1, final_filepath_string_temp.length()-1);
			}
			else {
				final_filepath_string=final_filepath_string_temp;
			}
			
			File final_filepath = new File(final_filepath_string);
			if (!final_filepath.exists()) { //if there is no such filepath, make new directory
				final_filepath.mkdirs();
	        }

			String final_path_file_string = final_filepath_string+"/"+filename+".csv";
		    
			File temp_path_file = new File(temp_path_file_string);
		    File final_path_file = new File(final_path_file_string);

		    if(temp_path_file.exists())
		    {
		    	temp_path_file.renameTo(final_path_file);
		    	if(!final_path_file.exists()) {
		    		System.out.println("java file move fail");
		    	}
		    	else {
					System.out.println("csv export success");
		    	}
		    }
		    else {
		    	System.out.println("mysql export fail");
		    }
			//rtn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} 
		finally {
			disconnect();
		}
		return true;
	}

	public boolean exportIndexCSV(String final_filepath_string, String filename){
		connect();
		String sql = "select 'id', 'index_lat','index_lng','index_earthquake_reported_datetime','index_sum_times','index_count_times','index_sum_q5','index_count_q5','index_sum_q6','index_count_q6','index_sum_q8','index_count_q8','index_sum_q11','index_count_q11','index_sum_q12','index_count_q12','index_sum_q13','index_count_q13','index_sum_q16','index_count_q16','index_cws','index_cdi' "
				+ "union all "
				+ "select * from index_sgn_earthquake_report "
				+ "order by index_earthquake_reported_datetime desc, id desc, index_lat, index_lng "
				+ "INTO OUTFILE '"+ mysql_exportable_directory + filename + ".csv' "
				+ "FIELDS ENCLOSED BY '\"'" 
				+ "TERMINATED BY ';'" 
				+ "ESCAPED BY '\"'" 
				+ "LINES TERMINATED BY '\r\n';";
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rtn = pstmt.executeQuery();
			//temporarily save csv file at the mysql_exportable_directory
			
			String temp_path_file_string = mysql_exportable_directory+filename+".csv";
			
			String final_filepath_string_temp = final_filepath_string.replace("\\", "/");
			if(final_filepath_string_temp.endsWith("/")) {
				final_filepath_string=final_filepath_string_temp.substring(1, final_filepath_string_temp.length()-1);
			}
			else {
				final_filepath_string=final_filepath_string_temp;
			}
			
			File final_filepath = new File(final_filepath_string);
			if (!final_filepath.exists()) { //if there is no such filepath, make new directory
				final_filepath.mkdirs();
	        }

			String final_path_file_string = final_filepath_string+"/"+filename+".csv";

		    
			System.out.println(temp_path_file_string);
			System.out.println(final_path_file_string);
			
			File temp_path_file = new File(temp_path_file_string);
		    File final_path_file = new File(final_path_file_string);

		    if(temp_path_file.exists())
		    {
		    	temp_path_file.renameTo(final_path_file);
		    	if(!final_path_file.exists()) {
		    		System.out.println("java file move fail");
		    	}
		    	else {
					System.out.println("csv export success");
		    	}
		    }
		    else {
		    	System.out.println("mysql export fail");
		    }
			//rtn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} 
		finally {
			disconnect();
		}
		return true;
	}
	
	public boolean exportIndexCSV(String final_filepath_string, String filename, Timestamp from_datetime, Timestamp to_datetime){
		connect();
		String sql = "select 'id', 'index_lat','index_lng','index_earthquake_reported_datetime','index_sum_times','index_count_times','index_sum_q5','index_count_q5','index_sum_q6','index_count_q6','index_sum_q8','index_count_q8','index_sum_q11','index_count_q11','index_sum_q12','index_count_q12','index_sum_q13','index_count_q13','index_sum_q16','index_count_q16','index_cws','index_cdi' "
				+ "union all "
				+ "select * from index_sgn_earthquake_report "
				+ "where index_earthquake_reported_datetime between ? and ? "
				+ "order by index_earthquake_reported_datetime desc, id desc, index_lat, index_lng "
				+ "INTO OUTFILE '"+ mysql_exportable_directory + filename + ".csv' "
				+ "FIELDS ENCLOSED BY '\"'" 
				+ "TERMINATED BY ';'" 
				+ "ESCAPED BY '\"'" 
				+ "LINES TERMINATED BY '\r\n';";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setTimestamp(1,from_datetime);
			pstmt.setTimestamp(2,to_datetime);
			ResultSet rtn = pstmt.executeQuery();
			//temporarily save csv file at the mysql_exportable_directory
			
			String temp_path_file_string = mysql_exportable_directory+filename+".csv";
			
			String final_filepath_string_temp = final_filepath_string.replace("\\", "/");
			if(final_filepath_string_temp.endsWith("/")) {
				final_filepath_string=final_filepath_string_temp.substring(1, final_filepath_string_temp.length()-1);
			}
			else {
				final_filepath_string=final_filepath_string_temp;
			}
			
			File final_filepath = new File(final_filepath_string);
			if (!final_filepath.exists()) { //if there is no such filepath, make new directory
				final_filepath.mkdirs();
	        }

			String final_path_file_string = final_filepath_string+"/"+filename+".csv";
		    
			File temp_path_file = new File(temp_path_file_string);
		    File final_path_file = new File(final_path_file_string);

		    if(temp_path_file.exists())
		    {
		    	temp_path_file.renameTo(final_path_file);
		    	if(!final_path_file.exists()) {
		    		System.out.println("java file move fail");
		    	}
		    	else {
					System.out.println("csv export success");
		    	}
		    }
		    else {
		    	System.out.println("mysql export fail");
		    }
			//rtn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} 
		finally {
			disconnect();
		}
		return true;
	}

	public Admin_UserData login(Admin_UserData user) {
		connect();
        String username = user.getUsername();    
        String password = user.getPassword();

		String sql = "select * from admin_sgn_earthquake where username='" + username
                + "' AND password='" + password + "'";

		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rtn = pstmt.executeQuery();

			//if user exists set the isValid variable to true
			if (rtn.next()) {
	           user.setValid(true);
			}
	        // if user does not exist set the isValid variable to false
			else {
				user.setValid(false);
			}
		}

		catch (Exception ex) 
		{
			System.out.println("Log In failed: An Exception has occurred! " + ex);
		}
		//some exception handling
		
		finally 
		{
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (Exception e) {}
					pstmt = null;
			}
			/*disconnect();*/
		}
		return user;
		}
	}

