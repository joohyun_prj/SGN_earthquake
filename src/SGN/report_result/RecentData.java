package SGN.report_result;

import java.sql.Timestamp;

public class RecentData {
	
	private double lat;
	private double lng;
	private double cdi;
	private Timestamp report_datetime;
	
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public double getCdi() {
		return cdi;
	}
	public void setCdi(double cdi) {
		this.cdi = cdi;
	}
	public Timestamp getReport_datetime() {
		return report_datetime;
	}
	public void setReport_datetime(java.sql.Timestamp report_datatime) {
		this.report_datetime = report_datatime;
	}
	
}
