/*
 
 this object is used to compute CDI.
 there are a few Mid_compute_cdi function in admin.java file.(overwriting)
 particularly this object is used in Mid_compute_function that get only one parameter
 as raw report data (one row of sgn_earthquake_report table).    
 
 */

package SGN.report_result;

public class ComputeCdiData {
	double q1;
	double final_q4;
	Double q5;
	Double q6;
	Double q8;
	Double q11;
	Double q12;
	Double q13;
	Double q16;
	public double getQ1() {
		return q1;
	}
	public void setQ1(double q1) {
		this.q1 = q1;
	}
	public double getFinal_q4() {
		return final_q4;
	}
	public void setFinal_q4(double final_q4) {
		this.final_q4 = final_q4;
	}
	public Double getQ5() {
		return q5;
	}
	public void setQ5(Double q5) {
		this.q5 = q5;
	}
	public Double getQ6() {
		return q6;
	}
	public void setQ6(Double q6) {
		this.q6 = q6;
	}
	public Double getQ8() {
		return q8;
	}
	public void setQ8(Double q8) {
		this.q8 = q8;
	}
	public Double getQ11() {
		return q11;
	}
	public void setQ11(Double q11) {
		this.q11 = q11;
	}
	public Double getQ12() {
		return q12;
	}
	public void setQ12(Double q12) {
		this.q12 = q12;
	}
	public Double getQ13() {
		return q13;
	}
	public void setQ13(Double q13) {
		this.q13 = q13;
	}
	public Double getQ16() {
		return q16;
	}
	public void setQ16(Double q16) {
		this.q16 = q16;
	}
}
