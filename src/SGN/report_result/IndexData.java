package SGN.report_result;

public class IndexData {

	private double index_id;
	private double index_lat;
	private double index_lng;
	private java.sql.Timestamp index_earthquake_reported_datetime;
	
	private double index_sum_times;
	private double index_count_times;
	private double index_sum_q5;
	private double index_count_q5;
	private double index_sum_q6;
	private double index_count_q6;
	private double index_sum_q8;
	private double index_count_q8;
	private double index_sum_q11;
	private double index_count_q11;
	private double index_sum_q12;
	private double index_count_q12;
	private double index_sum_q13;
	private double index_count_q13;
	private double index_sum_q16;
	private double index_count_q16;
	private double index_cws;
	private double index_cdi;
	public double getIndex_id() {
		return index_id;
	}
	public void setIndex_id(double index_id) {
		this.index_id = index_id;
	}
	public double getIndex_lat() {
		return index_lat;
	}
	public void setIndex_lat(double index_lat) {
		this.index_lat = index_lat;
	}
	public double getIndex_lng() {
		return index_lng;
	}
	public void setIndex_lng(double index_lng) {
		this.index_lng = index_lng;
	}
	public java.sql.Timestamp getIndex_earthquake_reported_datetime() {
		return index_earthquake_reported_datetime;
	}
	public void setIndex_earthquake_reported_datetime(java.sql.Timestamp index_earthquake_reported_datetime) {
		this.index_earthquake_reported_datetime = index_earthquake_reported_datetime;
	}
	public double getIndex_sum_times() {
		return index_sum_times;
	}
	public void setIndex_sum_times(double index_sum_times) {
		this.index_sum_times = index_sum_times;
	}
	public double getIndex_count_times() {
		return index_count_times;
	}
	public void setIndex_count_times(double index_count_times) {
		this.index_count_times = index_count_times;
	}
	public double getIndex_sum_q5() {
		return index_sum_q5;
	}
	public void setIndex_sum_q5(double index_sum_q5) {
		this.index_sum_q5 = index_sum_q5;
	}
	public double getIndex_count_q5() {
		return index_count_q5;
	}
	public void setIndex_count_q5(double index_count_q5) {
		this.index_count_q5 = index_count_q5;
	}
	public double getIndex_sum_q6() {
		return index_sum_q6;
	}
	public void setIndex_sum_q6(double index_sum_q6) {
		this.index_sum_q6 = index_sum_q6;
	}
	public double getIndex_count_q6() {
		return index_count_q6;
	}
	public void setIndex_count_q6(double index_count_q6) {
		this.index_count_q6 = index_count_q6;
	}
	public double getIndex_sum_q8() {
		return index_sum_q8;
	}
	public void setIndex_sum_q8(double index_sum_q8) {
		this.index_sum_q8 = index_sum_q8;
	}
	public double getIndex_count_q8() {
		return index_count_q8;
	}
	public void setIndex_count_q8(double index_count_q8) {
		this.index_count_q8 = index_count_q8;
	}
	public double getIndex_sum_q11() {
		return index_sum_q11;
	}
	public void setIndex_sum_q11(double index_sum_q11) {
		this.index_sum_q11 = index_sum_q11;
	}
	public double getIndex_count_q11() {
		return index_count_q11;
	}
	public void setIndex_count_q11(double index_count_q11) {
		this.index_count_q11 = index_count_q11;
	}
	public double getIndex_sum_q12() {
		return index_sum_q12;
	}
	public void setIndex_sum_q12(double index_sum_q12) {
		this.index_sum_q12 = index_sum_q12;
	}
	public double getIndex_count_q12() {
		return index_count_q12;
	}
	public void setIndex_count_q12(double index_count_q12) {
		this.index_count_q12 = index_count_q12;
	}
	public double getIndex_sum_q13() {
		return index_sum_q13;
	}
	public void setIndex_sum_q13(double index_sum_q13) {
		this.index_sum_q13 = index_sum_q13;
	}
	public double getIndex_count_q13() {
		return index_count_q13;
	}
	public void setIndex_count_q13(double index_count_q13) {
		this.index_count_q13 = index_count_q13;
	}
	public double getIndex_sum_q16() {
		return index_sum_q16;
	}
	public void setIndex_sum_q16(double index_sum_q16) {
		this.index_sum_q16 = index_sum_q16;
	}
	public double getIndex_count_q16() {
		return index_count_q16;
	}
	public void setIndex_count_q16(double index_count_q16) {
		this.index_count_q16 = index_count_q16;
	}
	public double getIndex_cws() {
		return index_cws;
	}
	public void setIndex_cws(double index_cws) {
		this.index_cws = index_cws;
	}
	public double getIndex_cdi() {
		return index_cdi;
	}
	public void setIndex_cdi(double index_cdi) {
		this.index_cdi = index_cdi;
	}
}
