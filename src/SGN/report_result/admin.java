package SGN.report_result;

import java.util.ArrayList;

public class admin {
	
	   public IndexData Mid_compute_cdi (GroupData gd) {
	         
	        IndexData rtn = new IndexData();
	        
	        rtn.setIndex_sum_times(gd.getSum_times());
	        rtn.setIndex_sum_q5(gd.getSum_q5());
	        rtn.setIndex_sum_q6(gd.getSum_q6());
	        rtn.setIndex_sum_q8(gd.getSum_q8());
	        rtn.setIndex_sum_q11(gd.getSum_q11());
	        rtn.setIndex_sum_q12(gd.getSum_q12());
	        rtn.setIndex_sum_q13(gd.getSum_q13());
	        rtn.setIndex_sum_q16(gd.getSum_q16());
	            
	        rtn.setIndex_count_times(gd.getCount_times());
	        rtn.setIndex_count_q5(gd.getCount_q5());
	        rtn.setIndex_count_q6(gd.getCount_q6());
	        rtn.setIndex_count_q8(gd.getCount_q8());
	        rtn.setIndex_count_q11(gd.getCount_q11());
	        rtn.setIndex_count_q12(gd.getCount_q12());
	        rtn.setIndex_count_q13(gd.getCount_q13());
	        rtn.setIndex_count_q16(gd.getCount_q16());
	        
	        double temp_cws = rtn.getIndex_sum_times()/rtn.getIndex_count_times();
	        System.out.println(rtn.getIndex_sum_times());
	        System.out.println(rtn.getIndex_count_times());
	        if(rtn.getIndex_count_q5()!=0) {
	        	temp_cws+=rtn.getIndex_sum_q5()/rtn.getIndex_count_q5();
	        }
	        if(rtn.getIndex_count_q6()!=0) {
	        	temp_cws+=rtn.getIndex_sum_q6()/rtn.getIndex_count_q6();
	        }
	        if(rtn.getIndex_count_q8()!=0) {
	        	temp_cws+=rtn.getIndex_sum_q8()/rtn.getIndex_count_q8();
	        }
	        if(rtn.getIndex_count_q11()!=0) {
	        	temp_cws+=rtn.getIndex_sum_q11()/rtn.getIndex_count_q11();
	        }
	        if(rtn.getIndex_count_q12()!=0) {
	        	temp_cws+=rtn.getIndex_sum_q12()/rtn.getIndex_count_q12();
	        }
	        if(rtn.getIndex_count_q13()!=0) {
	        	temp_cws+=rtn.getIndex_sum_q13()/rtn.getIndex_count_q13();
	        }
	        if(rtn.getIndex_count_q16()!=0) {
	        	temp_cws+=rtn.getIndex_sum_q16()/rtn.getIndex_count_q16();
	        }
	        /*CWS = (sum of q1*q4)/(count of q1*q4) + (sum of q5)/(count of q5) + (sum of q6)/(count of q6) + (sum of q8)/(count of q8) + (sum of q11)/(count of q11) + (sum of q12)/(count of q12) + (sum of q13)/(count of q13) + (sum of q16)/(count of q16)*/
	        rtn.setIndex_cws(temp_cws);
	        if(rtn.getIndex_cws()==0) {
	        	/*if cws equals to 0, cdi equals to 1*/
	        	rtn.setIndex_cdi(1.0);
	        }
	        else {
	        	if(3.40 * Math.log(rtn.getIndex_cws()) - 4.38 < 2) {
	        		/*unless cws equals 0, valid minimum cdi value is 2*/
	        		rtn.setIndex_cdi(2.0);
	        	}
	        	else if (3.40 * Math.log(rtn.getIndex_cws()) - 4.38 > 9) {
	        		rtn.setIndex_cdi(9.0);
	        		/*valid maximum cdi value is 9*/
	        	}
	        	else {
	        		double temp_cdi = 3.40 * Math.log(rtn.getIndex_cws()) - 4.38;
	        		rtn.setIndex_cdi(Math.round(temp_cdi*10)/10.0);
	        		/*CDI = 3.40 ln(CWS) - 4.38*/
	        		/*CDI must be rounded to first decimal point*/
	        	}
	        }
	        return rtn;
	   }
	   
	   public double Mid_compute_cdi (ComputeCdiData ccd) {
	        double cws=0;
	        double cdi=0;
	        
	        double adjusted_q1 = ccd.getQ1();
	        if(ccd.getFinal_q4()!=1) {
	        	adjusted_q1 *= ccd.getFinal_q4();
	        }
	        cws+=adjusted_q1;
	        
	        if(!(ccd.getQ5().equals(null))) {
	        	cws+=ccd.getQ5();
	        }

	        if(!(ccd.getQ6().equals(null))) {
	        	cws+=ccd.getQ6();
	        }
	        
	        if(!(ccd.getQ8().equals(null))) {
	        	cws+=ccd.getQ8();
	        }
	        
	        if(!(ccd.getQ11().equals(null))) {
	        	cws+=ccd.getQ11();
	        }

	        if(!(ccd.getQ12().equals(null))) {
	        	cws+=ccd.getQ12();
	        }

	        if(!(ccd.getQ13().equals(null))) {
	        	cws+=ccd.getQ13();
	        }

	        if(!(ccd.getQ16().equals(null))) {
	        	cws+=ccd.getQ16();
	        }
	        /*CWS = (sum of q1*q4)/(count of q1*q4) + (sum of q5)/(count of q5) + (sum of q6)/(count of q6) + (sum of q8)/(count of q8) + (sum of q11)/(count of q11) + (sum of q12)/(count of q12) + (sum of q13)/(count of q13) + (sum of q16)/(count of q16)*/

	        
	        if(cws==0) {
	        	cdi=1.0;
	        	/*if cws equals to 0, cdi equals to 1*/
	        }
	        else {
	        	if(3.40 * Math.log(cws) - 4.38 < 2) {
	        		cdi=2.0;
	        		/*unless cws equals 0, valid minimum cdi value is 2*/
	        	}
	        	else if(3.40 * Math.log(cws) - 4.38 > 9) {
	        		cdi=9.0;
	        		/*valid maximum cdi value is 9*/
	        	}
	        	else {
	        		cdi=3.40 * Math.log(cws) - 4.38;
	        		/*CDI = 3.40 ln(CWS) - 4.38*/
	        		/*CDI must be rounded to first decimal point*/
	        	}
	        }
	        return Math.round(cdi*10)/10.0;
	   }
	
	public IndexData Mid_compute_cdi (GroupData gd,IndexData in_d) {
		IndexData rtn = new IndexData();
		in_d.setIndex_sum_times(in_d.getIndex_sum_times() + gd.getSum_times());
		in_d.setIndex_sum_q5(in_d.getIndex_sum_q5() + gd.getSum_q5());
		in_d.setIndex_sum_q6(in_d.getIndex_sum_q6() + gd.getSum_q6());
		in_d.setIndex_sum_q8(in_d.getIndex_sum_q8() + gd.getSum_q8());
		in_d.setIndex_sum_q11(in_d.getIndex_sum_q11() + gd.getSum_q11());
		in_d.setIndex_sum_q12(in_d.getIndex_sum_q12() + gd.getSum_q12());
		in_d.setIndex_sum_q13(in_d.getIndex_sum_q13() + gd.getSum_q13());
		in_d.setIndex_sum_q16(in_d.getIndex_sum_q16() + gd.getSum_q16());

		in_d.setIndex_count_times(in_d.getIndex_count_times() + gd.getCount_times());
		in_d.setIndex_count_q5(in_d.getIndex_count_q5() + gd.getCount_q5());
		in_d.setIndex_count_q6(in_d.getIndex_count_q6() + gd.getCount_q6());
		in_d.setIndex_count_q8(in_d.getIndex_count_q8() + gd.getCount_q8());
		in_d.setIndex_count_q11(in_d.getIndex_count_q11() + gd.getCount_q11());
		in_d.setIndex_count_q12(in_d.getIndex_count_q12() + gd.getCount_q12());
		in_d.setIndex_count_q13(in_d.getIndex_count_q13() + gd.getCount_q13());
		in_d.setIndex_count_q16(in_d.getIndex_count_q16() + gd.getCount_q16());
		
        double temp_cws = rtn.getIndex_sum_times()/rtn.getIndex_count_times();
        if(in_d.getIndex_count_q5()!=0) {
        	temp_cws+=in_d.getIndex_sum_q5()/in_d.getIndex_count_q5();
        }
        if(in_d.getIndex_count_q6()!=0) {
        	temp_cws+=in_d.getIndex_sum_q6()/in_d.getIndex_count_q6();
        }
        if(in_d.getIndex_count_q8()!=0) {
        	temp_cws+=in_d.getIndex_sum_q8()/in_d.getIndex_count_q8();
        }
        if(in_d.getIndex_count_q11()!=0) {
        	temp_cws+=in_d.getIndex_sum_q11()/in_d.getIndex_count_q11();
        }
        if(in_d.getIndex_count_q12()!=0) {
        	temp_cws+=in_d.getIndex_sum_q12()/in_d.getIndex_count_q12();
        }
        if(in_d.getIndex_count_q13()!=0) {
        	temp_cws+=in_d.getIndex_sum_q13()/in_d.getIndex_count_q13();
        }
        if(in_d.getIndex_count_q16()!=0) {
        	temp_cws+=in_d.getIndex_sum_q16()/in_d.getIndex_count_q16();
        }
		
		in_d.setIndex_cws(temp_cws);
		
		if(in_d.getIndex_cws()==0) {
			in_d.setIndex_cdi(1.0);
		}
		else {
			if(3.40 * Math.log(in_d.getIndex_cws()) - 4.38 < 2){
				in_d.setIndex_cdi(2.0);
			}
			else if(3.40 * Math.log(in_d.getIndex_cws()) - 4.38 > 9){
				in_d.setIndex_cdi(9.0);
			}
			else {
				double temp_cdi = 3.40 * Math.log(in_d.getIndex_cws()) - 4.38;
				in_d.setIndex_cdi(Math.round(temp_cdi*10)/10.0);	
			}
		}
		rtn = in_d;
		return rtn;
	}
}
